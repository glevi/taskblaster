import os
from contextlib import contextmanager
from pathlib import Path


def color(string, fg=None, bg=None):
    import click
    return click.style(string, fg=fg, bg=bg)


@contextmanager
def workdir(directory):
    cwd = Path.cwd()
    try:
        os.chdir(directory)
        yield
    finally:
        os.chdir(cwd)


def normalize(string):
    # The string must be a valid graphviz node name, so we replace some chars.
    # Also we put an x in front to ensure that the starting char
    # is alphabetic.
    string = string.replace('/', '__slash__').replace('-', '__dash__')
    return f'x{string}'


def tree_to_graphviz_text(tree):
    tokens = ['digraph tasks {']

    graph = {}

    for node in tree.nodes_topological():
        ancestors = tree.registry.ancestry.ancestors(node.name)
        graph[node.name] = ancestors

    for name in graph:
        graphviz_name = normalize(name)
        tokens.append(f'  {graphviz_name} [label="{name}"]')

    for descendant, ancestors in graph.items():
        assert descendant in graph
        node2 = normalize(descendant)

        for ancestor in ancestors:
            assert ancestor in graph
            node1 = normalize(ancestor)
            tokens.append(f'  {node1} -> {node2}')

    tokens.append('}')
    return '\n'.join(tokens)
