import json
import typing
from pathlib import Path
from abc import ABC, abstractmethod
from taskblaster.hashednode import PackedReference
from taskblaster.encoding import encode_object, decode_object
from taskblaster import JSONCodec


class TBUserError(Exception):
    pass


class NullCodec(JSONCodec):
    def decode(self, dct):
        return dct

    def encode(self, obj):
        clsname = type(obj).__name__
        raise TypeError(f'No encoding rule for type {clsname}')


class ExtensibleCodec(JSONCodec):
    def __init__(self, usercodec):
        self.usercodec = usercodec

    def encode(self, obj):
        if hasattr(obj, 'tb_encode'):
            return encode_object(obj)
        return self.usercodec.encode(obj)

    def decode(self, dct):
        if '__tb_enc__' in dct:
            return decode_object(dct)
        return self.usercodec.decode(dct)


class JSONProtocol:
    def __init__(self, usercodec=None):
        if usercodec is None:
            usercodec = NullCodec()

        self.codec = ExtensibleCodec(usercodec)

    def serialize_inputs(self, obj: typing.Any, name) -> bytes:
        # The taskblaster caching mechanism uses hashes of serialized objects.
        # This is the function which serializes those things.  We should be
        # quite careful that this doesn't change behaviour.
        #
        # Note that keys are sorted so equal dictionaries will hash equally.

        def default(obj):
            return self.encode_and_pack_references(obj, name=name)

        jsontext = json.dumps(obj, default=self.encode_and_pack_references,
                              sort_keys=True)
        return jsontext.encode('utf-8')

    def dumps(self, obj):
        return json.dumps(obj, default=self.codec.encode)

    def encode_and_pack_references(self, obj, _recursions=[0]):
        # If the encoder also ends up encoding its dependencies'
        # dependencies, then that's bad and we have a problem.
        # This is a little check that'll fail if this is called
        # recursively:
        assert _recursions[0] == 0
        _recursions[0] += 1

        try:
            if hasattr(obj, '_tb_pack'):
                return obj._tb_pack()
        finally:
            _recursions[0] -= 1
        return self.codec.encode(obj)

    def load_inputs_without_resolving_references(self, buf):
        decoder = NodeDecoder(self.codec)
        name, kwargs = json.loads(buf, object_hook=decoder.decode)
        references = decoder.references
        return name, kwargs, references

    def load_inputs_and_resolve_references(self, cache, entry):
        """Load inputs and resolve references into objects via cache."""
        # XXX Temporary compatibility shortcut, clean me up
        return self.load_inputs_and_resolve_references_with_digest(
            cache, entry)[:2]

    def load_inputs_and_resolve_references_with_digest(self, cache, entry):
        # XXX Should this be together with the mechanism for evaluating
        # relative paths to abspaths inside results?
        from taskblaster.digest import mkhash

        # This method decides how we hash an input.
        #  * First we hash the text of the inputfile.
        #
        #  * Then we decode the references inside the inputfile.  When doing
        #    so, the ReferenceDecoder remembers the hashes of all the refs
        #    it sees.  This gives us a {name: digest} mapping for all the refs,
        #
        #  * Then we hash the {name: digest} mapping
        #
        #  * Finally we combine the inputfile hash and the refmap hash,
        #    which is then the full inputhash which will change if any
        #    names or dependency digests should change.
        #
        # This logic is relatively essential so it should probably not
        # be nearly as "buried" as is the case here.

        json_digest, jsontext = entry.read_inputfile()
        unpacker = ReferenceDecoder(self.codec, cache)
        name, namespace = json.loads(jsontext,
                                     object_hook=unpacker.decode)
        digestmap = {name: digest.long
                     for name, digest in unpacker.name2digest.items()}
        digestmap_text = json.dumps(digestmap, sort_keys=True)
        digestmap_digest = mkhash(digestmap_text.encode('ascii'))
        final_digest = mkhash((json_digest + digestmap_digest).encode('ascii'))

        # XXXX refactor
        namespace.pop('__tb_dynamic_parent__', None)
        return name, namespace, final_digest

    def outputencoder(self, directory):
        return OutputEncoding(self.codec, directory)


class BaseNodeDecoder(ABC):
    """Helper class to determine dependencies while reading JSON.

    Since dependencies (input "kwargs") can be arbitrarily nested,
    determining the dependencies requires complicated looping and
    type checking.

    This class implements a JSON hook which, whenever it reads a
    dependency, stores it.  That way, the JSON read loop
    takes care of the complicated looping, and we build the
    dependencies as a side effect when loading.

    That is what this class implements.
    """
    def __init__(self, codec):
        self.codec = codec
        self.references = []

    def decode(self, dct):
        tb_type = dct.get('__tb_type__')
        if tb_type is not None:
            if tb_type == 'ref':
                digest = dct['name']
                index = dct['index']
                ref = self.decode_reference(digest, index)
                self.references.append(ref)
                return ref
            else:
                raise RuntimeError(f'unknown __tb_type__={tb_type}')

        return self.codec.decode(dct)

    @abstractmethod
    def decode_reference(self, digest, index):
        ...


class NodeDecoder(BaseNodeDecoder):
    def decode_reference(self, digest, index):
        return PackedReference(digest, index)


class ReferenceDecoder(BaseNodeDecoder):
    def __init__(self, codec, cache):
        super().__init__(codec)
        self.cache = cache

        self.name2digest = {}

    def decode_reference(self, name, index):
        entry = self.cache.entry(name)
        if index is None:
            assert 0  # XXX build record

        value = entry.output()
        assert isinstance(index, list)
        for subindex in index:
            try:
                value = value[subindex]
            except Exception as e:
                raise TBUserError(
                    f'Cannot index {value} with {subindex}: {e}.') from None

        # XXX improve trainwreck
        digest = self.cache.registry.index.node(name).digest
        assert digest != ''

        # If name was already stored (e.g. due depdendency multiplicity)
        # then they must all map to the same digest:
        assert self.name2digest.get(name, digest) == digest

        self.name2digest[name] = digest
        return value


class OutputEncoding:
    """Helper for passing files between tasks via JSON encoding/decoding.

    We don't want to persistently save absolute paths because they
    become wrong if anything is moved.  But we are okay with saving
    a reference such as "myfile.dat".  When loading the file (and
    for passing result objects to other tasks), the file should then
    be resolved relative to the location of the original task.

    For example suppose we have cachedir/mytask-12345/myfile.dat .

    The task returns Path('myfile.dat') which we serialize as myfile.dat.
    That way if we rename/move cachedir or mytask-12345, the information
    which we stored does not become wrong.

    Only at runtime when we load the resultfile do we evaluate myfile
    inside whatever cachedir it was loaded from – at that point it becomes
    an absolute path.

    Note also how the value of that path will not be used for any hashes
    or equality checks, since we track identity through the dependency graph.
    """

    def __init__(self, codec, directory):
        self.codec = codec
        self.directory = directory.absolute()

    def decode(self, dct):
        tbtype = dct.get('__tb_type__')
        if tbtype is None:
            return self.codec.decode(dct)
        elif tbtype == 'Path':
            assert set(dct) == {'__tb_type__', 'path'}
            # Should we require that the path is relative?
            return self.directory / dct['path']

        raise RuntimeError(f'bad tbtype={tbtype!r}')

    def encode(self, obj):
        if isinstance(obj, Path) and not obj.is_absolute():
            return {'__tb_type__': 'Path', 'path': str(obj)}
        return self.codec.encode(obj)

    def loads(self, jsontext):
        return json.loads(jsontext, object_hook=self.decode)

    def dumps(self, obj):
        # sort keys or not?  For consistency with hashable inputs,
        # we could sort the keys.  But we don't need to, because this
        # is for storing outputs.  We'll sort them.
        return json.dumps(obj, default=self.encode, sort_keys=True)
