class Task:
    def __init__(self, name, target, kwargs, dynamical, is_workflow=False):
        self.name = name
        self.target = target
        self.kwargs = kwargs
        self.dynamical = dynamical
        self.is_workflow = is_workflow

    def signature(self):
        tokens = ', '.join(
            f'{name}={value!r}' for name, value in self.kwargs.items())
        return f'{self.target}({tokens})'

    def __repr__(self):
        sig = self.signature()
        return f'Task({self.name}, {sig})'

    def _node(self, cache, dynamic_parent=None):
        from taskblaster.hashednode import Node
        return Node.new(cache.json_protocol, self.target,
                        dct=self.kwargs, name=self.name,
                        dynamic_parent=dynamic_parent)


# stage 1: task generation
#  save tasks with namerefs (unhashed).
#  we save/maintain the awaitcount using the namerefs.
#  when awaitcount is 0, a task can run

# stage 2: task execution
#  resolve dependencies: we can hash inputs/outputs of dependencies
#  as we want, and store that.  This allows checking/invalidating
#  if anything changes.
#  hash needs to be saved somewhere; it could be part of the output
#  ("record") since next task won't start until current task is done
#  anyway,


def new_workflowtask(workflow, fullname):
    # XXX This will be <run_path>.ActualClassName.  We need a mechanism
    # to have the actual file written down and (safely) get stuff from there.
    target = 'taskblaster.runner.run_workflow'
    kwargs = {'workflow': workflow}
    # target = f'{wfclass.__module__}.{wfclass.__name__}'
    # kwargs = {name: getattr(workflow, name) for name in workflow._inputvars}
    task = Task(fullname, target, kwargs, False, is_workflow=True)
    return task
