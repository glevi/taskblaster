from dataclasses import dataclass
from typing import Set
from pathlib import Path
import sqlite3

from taskblaster.state import State
from taskblaster.digest import Digest, unhashed
from taskblaster.workers import Workers


@dataclass
class IndexNode:
    name: str  # task name (i.e., normalized path)
    state: State  # one-letter representation of state
    awaitcount: int  # how many direct ancestors are not done?
    workerclass: str
    argtext: str  # possibly ellipsized description text (for viewing)
    digest: Digest

    @classmethod
    def fromrow(cls, row):
        # Defines how registry rows are converted into IndexNode objects.
        digeststr = row[5]
        if digeststr == '':
            digest = unhashed
        else:
            digest = Digest(digeststr)

        return cls(
            name=row[0],
            state=State(row[1]),
            awaitcount=row[2],
            workerclass=row[3],
            argtext=row[4],
            digest=digest)

    def torow(self):
        return (self.name, self.state.value, self.awaitcount,
                self.workerclass, self.argtext, self.digest.long)


class Index:
    def __init__(self, conn):
        self.conn = conn

    @classmethod
    def initialize(cls, conn):
        conn.execute("""
CREATE TABLE IF NOT EXISTS registry (
    name VARCHAR(512) PRIMARY KEY,
    state CHAR(1),
    awaitcount INTEGER,
    workerclass VARCHAR(512),
    argtext VARCHAR(80),
    digest CHAR(64)
)""")

        indices = {
            'state_index': 'registry(state)',
            'name_index': 'registry(name)',
            'awaitcount_index': 'registry(awaitcount)',
            'workerclass_index': 'registry(workerclass)',
            'ready_index': 'registry(state, awaitcount, workerclass)',
            'digest_index': 'registry(digest)',
        }

        for indexname, target in indices.items():
            statement = f'CREATE INDEX IF NOT EXISTS {indexname} ON {target}'
            conn.execute(statement)

        """conflicts table keeps track of tasks with conflicts. I.e. tasks
           that are marked as done but which were excecuted with different
           input paramaters than in the current workflow.
        """
        conn.execute("""
CREATE TABLE IF NOT EXISTS conflicts (
    task_name VARCHAR(512) PRIMARY KEY,
    conflict CHAR(1),
    reason BLOB,
    buf BLOB)
    """)

        return cls(conn)

    def add(self, indexnode: IndexNode) -> None:
        query = 'INSERT INTO registry VALUES (?, ?, ?, ?, ?, ?)'
        self.conn.execute(query, indexnode.torow())

    def find_ready(self):
        state = 'q'  # XXX duplication

        query = ('SELECT * FROM registry WHERE '
                 'state=(?) AND awaitcount=(?)')
        cursor = self.conn.execute(query, (state, 0))
        result = cursor.fetchone()
        if result is None:
            raise Missing()

        return IndexNode.fromrow(result)

    def update_states(self, names, state):
        query = 'UPDATE registry SET state=(?) WHERE name=(?)'
        self.conn.executemany(query, [(state, name) for name in names])

    def update_digest(self, name, digest):
        query = 'UPDATE registry SET digest=(?) WHERE name=(?)'
        assert len(digest) == 64 or digest == ''
        self.conn.execute(query, (digest, name))

    def update_state(self, name, state):
        self.update_states([name], state)

    def update_awaitcount(self, name, awaitcount):
        query = 'UPDATE registry SET awaitcount=(?) WHERE name=(?)'
        self.conn.execute(query, (awaitcount, name))

    def count(self) -> int:
        cursor = self.conn.execute('SELECT COUNT(*) FROM registry')
        return cursor.fetchone()[0]

    def glob(self, patterns, states=None, name=None, pattern=None):
        # matching a/b should include itself (obviously)
        # matching a/b should also include a/b/c and other subdirectories.
        #
        # To get both, we pass pattern as well as pattern/*, matching either.
        query_args = list(patterns)
        query_args += [pattern + '/*' for pattern in patterns]

        # Is there a way to execute multiple globs or must we build
        # this potentially very long string?
        glob_condition = ' OR '.join(['name GLOB (?)'] * len(query_args))
        if not states:
            condition = glob_condition
        else:
            condition = f'({glob_condition}) AND state IN (?)'
            statechars = [state.value for state in states]

            questionmarks = ', '.join('?' * len(statechars))
            condition = f'({glob_condition}) AND state IN ({questionmarks})'

            # print(query_args)
            query_args += list(statechars)

        query = f'SELECT * FROM registry WHERE {condition} ORDER BY name'
        # We need (?, ?, ..., ?) as many as there are states selected.
        cursor = self.conn.execute(query, query_args)
        if not name and not pattern:
            return [IndexNode.fromrow(row) for row in cursor.fetchall()]
        # Selecting tasks with given name or folders that contain pattern
        # XXX Could be done more efficient
        output = []
        for row in cursor.fetchall():
            indxnode = IndexNode.fromrow(row)
            if name == indxnode.name.split('/')[-1]:
                output.append(indxnode)
            elif pattern:
                if pattern in indxnode.name:
                    output.append(indxnode)
        return output

    def nodes(self):
        cursor = self.conn.execute(
            'SELECT * FROM registry ORDER BY name')
        for row in cursor:
            yield IndexNode.fromrow(row)

    def _getnode(self, name):
        query = 'SELECT * FROM registry WHERE name=(?)'
        cursor = self.conn.execute(query, (name,))
        rows = cursor.fetchall()
        return rows

    def node_by_name(self, name: str) -> IndexNode:
        query = 'SELECT * FROM registry WHERE name=(?)'
        cursor = self.conn.execute(query, (name,))
        rows = cursor.fetchall()
        assert len(rows) == 1  # XXX?
        return IndexNode.fromrow(rows[0])

    def node(self, name: str) -> IndexNode:
        rows = self._getnode(name)
        if len(rows) != 1:
            raise Missing(name)
        return IndexNode.fromrow(rows[0])

    def contains(self, name: str) -> bool:
        rows = self._getnode(name)
        return bool(rows)

    def remove_multiple(self, names):
        query = 'DELETE FROM registry WHERE name=(?)'
        self.conn.executemany(query, [(name,) for name in names])

    def asdict(self):
        return {node.name: node for node in self.nodes()}

    def namedict(self):
        return {node.name: node.name for node in self.nodes()}


class Missing(LookupError):
    pass


class Ancestry:
    """Storage of vertices in dependency graph as node --> parent map."""

    tablename = 'dependencies'

    def __init__(self, conn):
        self.conn = conn

    @classmethod
    def initialize(cls, conn) -> None:
        table = cls.tablename
        conn.execute(f"""\
CREATE TABLE IF NOT EXISTS {table} (
    ancestor CHAR(64),
    descendant CHAR(64),
    PRIMARY KEY (ancestor, descendant)
)""")
        conn.execute('CREATE INDEX IF NOT EXISTS ancestor_index '
                     f'ON {table}(ancestor)')
        conn.execute('CREATE INDEX IF NOT EXISTS descendant_index '
                     f'ON {table}(descendant)')
        conn.execute('CREATE INDEX IF NOT EXISTS combined_index '
                     f'ON {table}(ancestor, descendant)')
        return cls(conn)

    def _all(self):
        query = f'SELECT * FROM {self.tablename}'
        return self.conn.execute(query).fetchall()

    def graph(self):
        tokens = ['digraph ancestors {']

        for ancestor, descendant in self._all():
            for node in [ancestor, descendant]:
                tokens.append(f'  "{node}" [label="{node[:6]}"]')

            tokens.append(f'  "{ancestor}" -> "{descendant}"')

        tokens.append('}')
        return '\n'.join(tokens)

    def add(self, parent_name: str, name: str) -> None:
        query = f'INSERT INTO {self.tablename} VALUES (?, ?)'
        self.conn.execute(query, (parent_name, name))

    def contains(self, parent_name: str, name: str) -> None:
        query = (f'SELECT ancestor, descendant FROM {self.tablename} '
                 'WHERE ancestor=(?) AND descendant=(?)')
        cursor = self.conn.execute(query, (parent_name, name))
        count = len(cursor.fetchall())
        assert count <= 1
        return bool(count)

    def remove(self, parent_name: str, name: str) -> None:
        if not self.contains(parent_name, name):
            raise KeyError(parent_name, name)
        query = (f'DELETE FROM {self.tablename} '
                 'WHERE ancestor=(?) AND descendant=(?)')
        self.conn.execute(query, (parent_name, name))

    def _get_any(self, which: str, other: str, name: str) -> Set[str]:
        query = f'SELECT {which} FROM {self.tablename} WHERE {other}=(?)'
        cursor = self.conn.execute(query, (name,))
        return {obj[0] for obj in cursor.fetchall()}

    def ancestors(self, name: str) -> Set[str]:
        return self._get_any('ancestor', 'descendant', name)

    def descendants(self, name: str) -> Set[str]:
        return self._get_any('descendant', 'ancestor', name)

    # High-level functions -- they don't quite belong here.
    # Move to registry or something
    def add_node(self, node):
        for parent_name in node.parents:
            self.add(parent_name, node.name)

    def remove_node(self, node):
        for parent_name in self.ancestors(node.name):
            self.remove(parent_name, node.name)


UNKNOWN_AWAITCOUNT = -1


class Registry:
    """Collection of tasks providing efficient access.

    The registry is a mapping from task IDs (hashes) to task locations
    (directories inside cache)."""

    def __init__(self, regfile: Path, timeout=60):
        self.regfile = regfile
        self.conn = Connection(regfile, timeout)

        with self.conn:
            Index.initialize(self.conn)
            Ancestry.initialize(self.conn)
            Workers.initialize(self.conn)

    def unrun(self, name):
        self.workers.remove_runlog(name)
        self.index.update_digest(name, '')
        self.update_state(name, State.new)
        self.clear_conflict(name)
        self._recursive_update_descendant_state(
            name, State.new, until_state=State.new, clear_conflict=True)

    def _recursive_update_descendant_state(self, nodename, state, until_state,
                                           clear_conflict=False):
        for descendant in self._recurse_descendants(nodename, until_state):
            self.update_state(descendant, state)
            if clear_conflict:
                self.clear_conflict(descendant)

    def cancel_descendants(self, nodename):
        self._recursive_update_descendant_state(
            nodename, State.cancel, State.cancel)

    def _recurse_descendants(self, nodename, until_state):
        for descendant in self.ancestry.descendants(nodename):
            node = self.index.node(descendant)
            if node.state == until_state:
                return

            yield from self._recurse_descendants(descendant, until_state)
            yield descendant

    def _new_indexnode(self, node, awaitcount, worker=None):
        return IndexNode(
            name=node.name,
            state=State.new,
            awaitcount=awaitcount,
            workerclass=worker,
            argtext=node.input_repr(),
            digest=unhashed,
        )

    def _awaitcount(self, name):
        parent_names = self.ancestry.ancestors(name)
        parent_states = [State(self.index.node(parent_name).state)
                         for parent_name in parent_names]

        # XXX crashes on orphans.
        return len([state for state in parent_states
                    if state != State.done])

    def add_or_update(self, node, worker, force_overwrite=False):
        return self._add_or_update(node, worker, force_overwrite)

    def _add_or_update(self, node, worker, force_overwrite):
        try:
            indexnode = self.index.node(node.name)
        except Missing:
            self.ancestry.add_node(node)
            awaitcount = self._awaitcount(node.name)
            indexnode = self._new_indexnode(node, awaitcount, worker)
            self.index.add(indexnode)
            return 'add', indexnode
        else:
            if force_overwrite:
                # XXX unduplicate
                self.ancestry.remove_node(indexnode)
                self.ancestry.add_node(node)
                awaitcount = self._awaitcount(node.name)
                indexnode = self._new_indexnode(node, awaitcount, worker)
                self.index.remove_multiple([node.name])
                self.workers.remove_runlog(node.name)
                self.index.add(indexnode)
                return 'update', indexnode
            elif indexnode.workerclass != worker:
                self.index.update_worker(node.name, worker)
                indexnode = self.index.node(node.name)
                return 'update', indexnode
            else:
                return 'have', indexnode

    def add(self, node, name):  # also: workerclass
        self.ancestry.add_node(node)

        # workerclass comes from workflow file and goes directly into
        # registry.  This means the tree is not sufficient to rebuild
        # the whole registry right now.  We should fix that.
        #
        # We'll have to write down the workers into files or something.
        # Probably we can have a single metadata file.

        awaitcount = self._awaitcount(node.name)

        indexnode = self._new_indexnode(node, awaitcount, 'local')

        self.index.add(indexnode)

    def find_ready(self):
        return self.index.find_ready()

    def parent_states(self, name):
        states = {}

        done_count = 0
        for parent_name in self.ancestry.ancestors(name):
            try:
                state = State(self.index.node(parent_name).state)
            except Missing:
                state = State.missing
            states[parent_name] = state
            if state == State.done:
                done_count += 1

        node = self.index.node(name)
        awaitcount = node.awaitcount
        assert awaitcount == len(states) - done_count or (
            awaitcount == UNKNOWN_AWAITCOUNT)
        return states

    def update_worker_state(self, name, myqueue_id, subworker_id, state,
                            exception=None):
        self.workers.update_worker_state(
            name, myqueue_id, subworker_id, state, exception=exception)

    def get_runlog_info(self, name):
        return self.workers.get_runlog_info(name)

    def update_running_tasks(self, myqueue_id, state):
        print(f'call update_running_tasks {myqueue_id} {state}')
        for task in self.workers.get_running_tasks_by_myqueue_id(myqueue_id):
            print('Updating task', task)
            self.update_state(task, state)

    def clear_conflict(self, name):
        query = "DELETE FROM conflicts WHERE task_name=(?)"
        self.conn.execute(query, (name,))

    def update_conflicts(self, name, conflict, reason="", buf=0):
        """ Updates the coflictlog. If row does not exist it adds it
        """
        query = (
            'INSERT INTO conflicts '
            '(task_name, conflict, reason, buf) VALUES (?, ?, ?, ?) '
            'ON CONFLICT(task_name) '
            'DO UPDATE SET conflict=?, reason=?, buf=?')
        self.conn.execute(query, (name, conflict, reason, buf,
                                  conflict, reason, buf))

    def get_conflict_info(self, name):
        query = (
            'SELECT conflict, reason, buf from conflicts WHERE task_name=(?)')
        rows = self.conn.execute(query, (name,))
        for row in rows:
            return row
        return 'n', "", 0

    def update_state(self, name, state):
        descendants = self.ancestry.descendants(name)
        indexnode = self.index.node(name)

        oldstate = State(indexnode.state)
        delta_readiness = (state == State.done) - (oldstate == State.done)

        self.index.update_state(indexnode.name, state.value)
        if state == State.new:
            self.workers.remove_runlog(indexnode.name)
        for descendant in descendants:
            descendant_indexnode = self.index.node(descendant)
            if descendant_indexnode.awaitcount == UNKNOWN_AWAITCOUNT:
                continue

            if delta_readiness != 0:
                self.index.update_awaitcount(
                    descendant_indexnode.name,
                    descendant_indexnode.awaitcount - delta_readiness)

    def remove_nodes(self, nodes):
        # This may create orphans which would therefore not have
        # a meaningful "awaitcount".  Although maybe they can
        # keep it and it's "dangling" -- after all their parents might
        # be created again.  But we'd need to update awaitcount
        # when things are unrun.
        for node in nodes:
            self.ancestry.remove_node(node)
            self.clear_conflict(node.name)
            self.workers.remove_runlog(node.name)
        self.index.remove_multiple([node.name for node in nodes])

    def contains(self, name):
        return self.index.contains(name)

    @property
    def index(self):
        # XXX we need to hide the database tables
        # so we can keep them in sync
        # assert self.conn is not None
        return Index(self.conn)

    @property
    def workers(self):
        return Workers(self.conn)

    @property
    def ancestry(self):
        assert self.conn is not None
        return Ancestry(self.conn)


class Connection:
    def __init__(self, filename, timeout):
        self.filename = filename
        self._conn = None
        self.timeout = timeout

    @property
    def execute(self):
        return self._conn.execute

    @property
    def executemany(self):
        return self._conn.executemany

    def __enter__(self):
        assert self._conn is None
        self._conn = self._connect()
        return self._conn

    def __exit__(self, type, value, tb):
        if type is None:
            action = 'COMMIT'
        else:
            action = 'ROLLBACK'
        self._conn.execute(action)
        self._conn = None

    def _connect(self):
        try:
            connection = sqlite3.connect(
                self.filename, timeout=self.timeout,
                detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES,
                isolation_level='EXCLUSIVE')
        except sqlite3.OperationalError as err:
            msg = f'Failed to open sqlite3-connection to {self.regfile}'
            raise RegistryError(msg) from err

        connection.execute('BEGIN EXCLUSIVE')
        return connection


class RegistryError(Exception):
    pass
