import taskblaster as tb


def fail(msg):
    raise ValueError(msg)


def ok(msg):
    return msg


@tb.workflow
class Workflow:
    msg = tb.var()

    @tb.task
    def ok(self):
        return tb.node('ok', msg=self.msg)

    @tb.task
    def dependsonok(self):
        return tb.node('ok', msg=self.ok)

    @tb.task
    def fail(self):
        return tb.node('fail', msg=self.msg)

    @tb.task
    def dependsonfail(self):
        return tb.node('sleep', msg=self.fail)


@tb.workflow
class CompositeWorkflow:
    msg = tb.var()

    @tb.task
    def hello(self):
        return tb.node('ok', msg=self.msg)

    @tb.subworkflow
    def subworkflow(self):
        return Workflow(msg=self.msg)

    @tb.subworkflow
    def subworkflow2(self):
        return Workflow(msg=self.hello)

    @tb.subworkflow
    def subworkflow3(self):
        return Workflow(msg=self.subworkflow.ok)


def workflow(rn):
    for i in range(2):
        msg = f'hello {i}'
        rn.with_subdirectory(str(i)).run_workflow(Workflow(msg=msg))
