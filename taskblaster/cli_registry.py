import click
from taskblaster.state import State
from taskblaster.cli import with_repo


@click.group()
def registry():  # noqa: F811
    """View or manipulate registry."""


@registry.command()
@with_repo
def build(repo):
    """Add all tasks in the tree to the registry."""
    import graphlib
    from taskblaster.entry import Entry

    registry = repo.cache.registry

    nodes = {}
    graph = {}

    for inputfile in repo.root.glob('tree/**/input.json'):
        entry = Entry(inputfile.parent, repo.cache.json_protocol)
        name = str(inputfile.parent.relative_to(repo.tree_path))
        node = entry.node(name)
        assert node.name == name

        nodes[name] = node
        graph[name] = set(node._parents)

    sorter = graphlib.TopologicalSorter(graph)

    for name in sorter.static_order():
        node = nodes[name]
        action, indexnode = registry.add_or_update(
            node=node, worker=None)  # XXX worker?

        if repo.cache.entry(name).has_output():
            state = State.done
        else:
            state = State.new

        registry.update_state(name, state)
        print(action, indexnode.name, indexnode.state)


@registry.command()
@with_repo
def ancestors(repo):
    print(repo.cache.registry.ancestry.graph())


@registry.command()
@with_repo
def ls(repo):
    registry = repo.cache.registry
    for node in registry.index.nodes():
        print(node.digest.short, node.name, node.state, node.awaitcount,
              node.workerclass, node.argtext)
