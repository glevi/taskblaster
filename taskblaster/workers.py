from taskblaster.state import State
from taskblaster.mqintegration import myqueue
from taskblaster.util import color
from taskblaster.state import State as TBState

"""
   Lifespan

   tb worker submit

      Will register a worker to workers table with status 'queued'

   tb worker ls

      Will query myqueue and update states of each worker

      Will list status of all workers, their subworkers, and their tasks.
      For each worker, it will list also all tasks from runlog.
      Will register a worker to workers table with status queue

   tb worker ls

      Will query myqueue to

      Will list status of all workers, their subworkers, and their tasks

"""

CREATE_TABLE_workers = """
CREATE TABLE IF NOT EXISTS workers (
    myqueue_id VARCHAR(12) PRIMARY KEY,
    wall_time INT,
    physical_queue VARCHAR(50),
    cores INT,
    subworker_size INT,
    subworker_count INT,
    state CHAR(1),
    submit_time timestamp NULL,
    start_time timestamp NULL,
    end_time timestamp NULL)"""

CREATE_TABLE_runlog = """
CREATE TABLE IF NOT EXISTS runlog (
    task_name VARCHAR(512) PRIMARY KEY,
    myqueue_id VARCHAR(12),
    subworker_id VARCHAR(512),
    exception VARCHAR(512) NULL,
    state CHAR(1),
    start_time timestamp NULL,
    end_time timestamp NULL)
    """


class Workers:
    def __init__(self, conn):
        self.conn = conn

    @classmethod
    def initialize(cls, conn):
        """Table manages synchronization between taskblaster and myqueue.

        myqueue_id has an unique identified, which links a task to a
        particular myqueue job.

        subworker_id is a unique name for the taskblaster worker,
        which is running the task.
        """
        conn.execute(CREATE_TABLE_workers)
        conn.execute(CREATE_TABLE_runlog)

        indices = {'runlog_index': 'runlog(task_name)',
                   'runlog_index2': 'runlog(myqueue_id)',
                   'runlog_index3': 'runlog(subworker_id)',
                   'worker_myqueue_id': 'workers(myqueue_id)'}

        for indexname, target in indices.items():
            statement = f'CREATE INDEX IF NOT EXISTS {indexname} ON {target}'
            conn.execute(statement)

        return cls(conn)

    def get_active_workers(self):
        class WorkerRow:
            def __init__(self, row):
                (self.myqueue_id, self.wall_time, self.physical_queue,
                 self.cores, self.subworker_size, self.subworker_count,
                 self.state, self.submit_time,
                 self.start_time, self.end_time) = row
        query = "SELECT * FROM workers WHERE state in ('q','h','r')"
        # qhr is definition of active per myqueue
        return [WorkerRow(row) for row in self.conn.execute(query)]

    def sync(self, repo):
        with myqueue() as queue:
            self._sync(repo, queue)

    def _sync(self, repo, queue):
        """
            If worker is active in task blaster (myqueue state 'queued', 'hold'
            or running'), query myqueue for its status:
              * If myqueue doesn't find task (user has removed it from
                myqueue manually), mark tb worker to failed state,
                and mark all of subworker *running* tasks as *Failed*.
              * If myqueue returns multiple tasks for one id,
                display a warning and utilize the last one on the list
                (should not happen).
              * If myqueue state is_bad() returns True, update all *running*
                subworker tasks to *Failed*.
              * In any case, update worker state with myqueue state.
        """

        from myqueue.selection import Selection

        for worker in self.get_active_workers():
            tasks = queue.select(Selection(ids=set([worker.myqueue_id])))
            if len(tasks) == 0:
                print('Warning: Cannot retrieve information from myqueue '
                      f'about task {worker.myqueue_id}. Marking tasks by '
                      'each subworker as failed, and worker as failed.')
                # Update exceptions to running tasks
                query = """\
UPDATE runlog SET exception=(?) WHERE EXISTS(SELECT * FROM registry
WHERE runlog.task_name=registry.name
  AND runlog.myqueue_id=(?)
  AND runlog.state='r')"""

                msg = 'Failed due to worker manually removed from myqueue.'
                self.conn.execute(query, (msg, worker.myqueue_id,))

                # This updates the registry (i.e. state of each node)
                repo.registry.update_running_tasks(worker.myqueue_id,
                                                   TBState.fail)
                # This updates the state of the worker
                query = 'UPDATE workers SET state=? WHERE myqueue_id=?'
                self.conn.execute(query, ('F', worker.myqueue_id))
                continue
            if len(tasks) > 1:
                job = worker.myqueue_id
                print(
                    f'Warning: myqueue returned multiple tasks for id {job}.')
            for task in reversed(tasks):
                if task.state.is_bad():
                    query = """\
UPDATE runlog SET exception=(?)
WHERE EXISTS(SELECT * FROM registry
WHERE runlog.task_name=registry.name
  AND runlog.myqueue_id=(?)
  AND registry.state='r')"""

                    msg = f'Failed due to worker condition: {str(task.state)}.'
                    self.conn.execute(query, (msg, worker.myqueue_id))
                    repo.registry.update_running_tasks(worker.myqueue_id,
                                                       TBState.fail)
                if task.state.value != worker.state:
                    query = 'UPDATE workers SET state=? WHERE myqueue_id=?'
                    self.conn.execute(query, (task.state.value,
                                              worker.myqueue_id))
                break

    def submit_workers(self, repo, dry_run, count, resources, max_tasks,
                       subworker_count, subworker_size):
        from taskblaster.mqintegration import (
            mq_worker_task, submit_manytasks)

        print('submit worker')
        workerdir = repo.root / 'runlogs'
        workerdir.mkdir(exist_ok=True)

        mqtasks = [mq_worker_task(directory=workerdir, resources=resources,
                                  worker_module=repo.run_module,
                                  max_tasks=max_tasks,
                                  subworker_size=subworker_size,
                                  subworker_count=subworker_count)
                   for _ in range(count)]
        submit_manytasks(mqtasks, dry_run=dry_run)
        print(mqtasks)
        for task in mqtasks:
            self.register_worker(task, subworker_size, subworker_count)

    def register_worker(self, mqtask, subworker_size, subworker_count):
        query = """
INSERT INTO workers (myqueue_id, wall_time, physical_queue,
cores, subworker_size, subworker_count,
state, submit_time) VALUES
(?, ?, ?, ?, ?, ?, ?,
DATETIME(CURRENT_TIMESTAMP, 'localtime'))"""

        r = mqtask.resources
        self.conn.execute(query, (mqtask.id, r.tmax, r.nodename,
                                  r.cores, subworker_size, subworker_count,
                                  'q'))

    def ls(self, cols='isW'):
        def status(s):
            return {'q': ('queued', 'yellow'),
                    'd': ('done', 'green'),
                    'r': ('running', 'bright_yellow'),
                    'u': ('undefined', 'bright_red'),
                    'h': ('hold', 'yellow'),
                    'F': ('FAILED', 'bright_red'),
                    'T': ('TIMEOUT', 'bright_red'),
                    'M': ('MEMORY', 'bright_red'),
                    'C': ('CANCELLED', 'bright_red')}[s]

        query = "SELECT * FROM workers"
        columns = {'i': ('myqueue id', 12, lambda x: x[0]),
                   's': ('status', 10, lambda x: status(x[6])),
                   'W': ('wall time', 10, lambda x: str(x[1]))}

        def adjust_cell(s, length):
            col = None
            if isinstance(s, tuple):
                s, col = s
            if len(s) > length:
                s = s[:length - 1] + "…"
            return color(s.ljust(length), col)

        # Header row
        for c in cols:
            name, length, _ = columns[c]
            print(color(adjust_cell(name, length), 'yellow'), end='')
        print()

        # Header line
        for c in cols:
            _, length, _ = columns[c]
            print(color('─' * (length - 1) + ' ', 'bright_yellow'), end='')
        print()

        # Rows
        for row in self.conn.execute(query):
            for c in cols:
                _, length, f = columns[c]
                print(adjust_cell(f(row), length), end='')
            print()
            myqueue_id = row[0]
            for task in self.get_tasks_by_myqueue_id(myqueue_id):
                print(task)

    def get_runlog_info(self, name):
        query = (
            "SELECT subworker_id, start_time, end_time, exception"
            " FROM runlog WHERE task_name=(?)")
        rows = self.conn.execute(query, (name,))
        for row in rows:
            return row
        return '', None, None, None

    def remove_runlog(self, name):
        query = "DELETE FROM runlog WHERE task_name=(?)"
        self.conn.execute(query, (name, ))

    def get_tasks_by_myqueue_id(self, myqueue_id):
        query = (
            "SELECT task_name "
            "FROM runlog, registry"
            " WHERE runlog.myqueue_id=(?)"
            " AND registry.name = runlog.task_name")
        return [row for row, in self.conn.execute(query, (myqueue_id, ))]

    def get_running_tasks_by_myqueue_id(self, myqueue_id):
        query = (
            "SELECT task_name "
            "FROM runlog, registry"
            " WHERE runlog.myqueue_id=(?)"
            " AND registry.name = runlog.task_name"
            " AND registry.state='r'")
        return [row for row, in self.conn.execute(query, (myqueue_id, ))]

    def update_worker_state(self, name, myqueue_id, subworker_id, state,
                            exception=None):
        """
           state='r', add the record and update start time
           state='d', update the end time
           state='F', update the end time
        """
        if state == State.run:
            assert exception is None
            query = ("INSERT INTO runlog "
                     "(task_name, myqueue_id, subworker_id, start_time)"
                     " VALUES (?, ?, ?,"
                     " DATETIME(CURRENT_TIMESTAMP,'localtime'))"
                     " ON CONFLICT(task_name)"
                     " DO UPDATE SET myqueue_id=?, subworker_id=?, "
                     " start_time=DATETIME(CURRENT_TIMESTAMP, 'localtime')")
            self.conn.execute(query, (name, myqueue_id, subworker_id,
                                      myqueue_id, subworker_id))
        else:
            query = (
                "UPDATE runlog"
                " SET exception=?, "
                " end_time=DATETIME(CURRENT_TIMESTAMP, 'localtime')"
                " WHERE task_name=(?)")
            self.conn.execute(query, (exception, name,))

    def find_running_myqueue_jobs(self):
        query = ("SELECT DISTINCT runlog.myqueue_id"
                 " FROM registry"
                 " LEFT JOIN runlog ON registry.name = runlog.task_name")
        return [row[0] for row in self.conn.execute(query)]
