from typing import Any
from pathlib import Path

from taskblaster.hashednode import Node, mkhash


class Entry:
    """Entry in file-based cache.

    Entry wraps a directory and provides access to functionality
    related to how a task is physically stored inside that directory."""

    inputname = 'input.json'
    outputname = 'output.json'
    flagname = 'flags.json'

    def __init__(self, directory, json_protocol):
        self.directory = Path(directory)
        self.json_protocol = json_protocol

    def __eq__(self, other):
        return self.directory == getattr(other, 'directory', None)

    def __neq__(self, other):
        return not self == other

    def __hash__(self):
        return hash(self.directory)

    def __repr__(self):
        return f'<Entry({self.directory})>'

    @classmethod
    def create(cls, node, directory, json_protocol, exist_ok=False):
        entry = cls(directory, json_protocol)
        if not exist_ok and entry.inputfile.exists():
            raise RuntimeError(f'Exists: {entry.inputfile}')

        entry.inputfile.write_bytes(node._buf)
        return entry

    @property
    def inputfile(self) -> Path:
        return self.directory / self.inputname

    @property
    def outputfile(self) -> Path:
        return self.directory / self.outputname

    def delete(self):
        assert self.inputfile.parent == self.directory
        assert self.outputfile.parent == self.directory

        self.inputfile.unlink(missing_ok=True)
        self.outputfile.unlink(missing_ok=True)

    def read_inputfile(self):
        buf = self.inputfile.read_bytes()
        digest = mkhash(buf)
        return digest, buf.decode('utf-8')

    def node(self, name) -> Node:
        return Node.fromdata(self.json_protocol, self.inputfile.read_bytes(),
                             name)

    def output(self) -> Any:
        output = self.outputfile.read_text()
        return self._hook.loads(output)

    def has_output(self):
        return self.outputfile.exists()

    @property
    def _hook(self):
        return self.json_protocol.outputencoder(self.directory)

    def dump_output(self, output):
        jsontext = self._hook.dumps(output)
        outbytes = jsontext.encode('utf-8')

        # We first write to out.json.part and then rename to out.json.
        # This means if and when out.json exists, it is guaranteed
        # intact.
        target = self.outputfile
        tmpfile = target.with_name(target.name + '.part')
        tmpfile.write_bytes(outbytes)
        tmpfile.rename(target)
        assert not tmpfile.exists()

    @property
    def flags(self):
        return Flags(self.directory / self.flagname)


class Flags:
    def __init__(self, path):
        self.path = path

    def read(self):
        if not self.path.exists():
            return set()
        txt = self.path.read_text()
        return set(txt.split())

    def write(self, flags):
        assert not isinstance(flags, str)
        flags = sorted(flags)

        if not flags:
            if self.path.exists():
                self.path.unlink()
                return

        for flag in flags:
            import re
            if re.search(r'\s', flag):
                raise ValueError(r'Flag {flag!r} contains whitespace')

        txt = ' '.join(flags)
        self.path.write_text(txt)
