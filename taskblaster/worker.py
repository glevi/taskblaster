from datetime import datetime
import traceback

from taskblaster.parallel import SerialCommunicator
from taskblaster.registry import Missing
from taskblaster.state import State
from taskblaster.util import workdir
from taskblaster.storage import TBUserError


class Panic(Exception):
    pass


class TaskContext:
    def __init__(self, comm, tb_comm, workername):
        self.comm = comm
        self.tb_comm = tb_comm
        self.workername = workername

    def parprint(self, *args, **kwargs):
        if self.tb_comm.rank == 0:
            print(*args, **kwargs)


class LoadedTask:
    def __init__(self, entry, name, target, kwargs, inputdigest):
        self.entry = entry
        self.name = name
        self.target = target
        self.kwargs = kwargs
        self.inputdigest = inputdigest

    def run(self, worker):
        function = worker.repo.import_task_function(self.target)

        if not callable(function):
            raise TypeError(f'Expected callable but got {function}')

        with workdir(self.entry.directory):
            kwargs = self.kwargs.copy()
            if getattr(function, '_tb_mpi', False):
                assert 'mpi' not in kwargs
                kwargs['mpi'] = TaskContext(
                    comm=worker.comm.usercomm(),
                    tb_comm=worker.comm,
                    workername=worker.name)

            # XXX Special case for running a workflow
            if self.target == 'taskblaster.runner.run_workflow':
                from taskblaster.runner import Runner
                rn = Runner(worker.repo, directory=self.entry.directory)
                # Need to be careful about MPI and ranks.
                output = function(rn, **kwargs)
            else:
                output = function(**kwargs)

            if worker.comm.rank == 0:
                self.entry.dump_output(output)

    def update_state(self, cache, state, myqueue_id, worker_name,
                     exception=None):
        with cache.registry.conn:

            if state == State.fail:
                cache.registry.cancel_descendants(self.name)

            cache.registry.update_state(self.name, state)

            cache.registry.update_worker_state(
                self.name, myqueue_id, worker_name, state, exception=exception)

            if state == State.done:
                # XXX We may want to assign a digest which is not inputdigest.
                cache.registry.index.update_digest(
                    self.name, self.inputdigest)


# XXX The inputdigest should be created from the actual digests
# of the inputs rather than just the encoded JSON, which contains
# the names.

# XXX here we need to save the hash of the inputs, where the
# namerefs are replaced by hashes.  That will work like before.
# But it's somewhat redundant since it only replaces the namerefs
# but otherwise is the same structure as the existing inputs.

# Then we hash the contents in that file, and that's the hash which
# we save to the registry.

# we need to dump a dictionary of {ref_id: hash}.
# Presumably we would save the digest to the registry,
# but then dump the actual

# Also: maybe some tasks are compared by return value,
# which is something that we can allow.  For example
# spacegroup might be 5, and remain 5 even if the inputs
# used to determine it change, and subsequent tasks should not
# be invalidated due to that.  Which means in the hashing
# it must be "5" that appears rather than a reference.

def exception_summary(exc):
    if exc is None:
        return None
    return f'{type(exc).__name__}: {exc}'


class Worker:
    # (Class name is not particularly accurate right now)
    MAX_TASKS_UNLIMITED = -1

    def __init__(self, repo, name='worker', selection=None, myqueue_id=None,
                 comm=SerialCommunicator(), max_tasks=MAX_TASKS_UNLIMITED):
        self.name = name
        self.repo = repo
        self.comm = comm

        if selection is None:
            selection = self._select_any()

        self.selection = selection
        self.cache = repo.cache
        self.registry = repo.registry
        self.max_tasks = max_tasks
        self.myqueue_id = myqueue_id

    def log(self, msg):
        if self.comm.rank:
            return

        now = datetime.now()
        timestamp = str(now).rsplit('.', 1)[0]
        print(f'[{timestamp} {self.name}] {msg}', flush=True)

    def _select_any(self):
        while True:
            yield self.cache.find_ready()

    def acquire_task(self):
        while True:
            if self.comm.rank == 0:
                loaded_task = self._acquire_task()
            else:
                loaded_task = None

            loaded_task = self.comm.broadcast_object(loaded_task)

            if loaded_task is None:
                raise Missing
            if loaded_task == 'PANIC':
                raise Panic
            if loaded_task == 'CONTINUE':
                continue
            break

        return loaded_task

    def _acquire_task(self):
        assert self.comm.rank == 0
        with self.cache.registry.conn:
            try:
                try:
                    indexnode = next(self.selection)
                except StopIteration:
                    raise Missing

                self.registry.update_state(indexnode.name, State.run)
                self.registry.update_worker_state(
                    indexnode.name, self.myqueue_id, self.name, State.run)

                entry = self.cache.entry(indexnode.name)
                protocol = self.cache.json_protocol

                (target, kwargs, inputdigest) = (
                    protocol.load_inputs_and_resolve_references_with_digest(
                        self.cache, entry))

                return LoadedTask(entry, indexnode.name,
                                  target, kwargs, inputdigest)
            except Missing:
                return
            except TBUserError as ex:
                self.log(
                    f'Error while initializing task {indexnode.name}: {ex}')
                self.registry.update_state(indexnode.name, State.fail)
                print(f'Updating the database with exception {str(ex)}')
                self.registry.update_worker_state(
                    indexnode.name, self.myqueue_id, self.name, State.fail,
                    exception=exception_summary(ex))
                return 'CONTINUE'
            except Exception as ex:
                self.registry.cancel_descendants(indexnode.name)
                self.registry.update_state(indexnode.name, State.fail)
                self.registry.update_worker_state(
                    indexnode.name, self.myqueue_id, self.name, State.fail,
                    exception=exception_summary(ex))
                print('Worker panic! Stopping.')
                print('Exception occurred while trying to initialize ')
                print('queued task to be run at the worker')
                print(traceback.format_exc())
            return 'PANIC'

    def main(self):
        self.log('Main loop')

        ntasks = 0
        while True:
            if self.max_tasks is not None and ntasks == self.max_tasks:
                self.log(f'Max tasks {ntasks} reached, end worker main loop')
                return

            ntasks += 1

            try:
                self.process_one_task()
            except Missing:
                self.log('No available tasks, end worker main loop')
                return
            except Panic:
                self.log('Worker terminating due to exception in task '
                         'initialization.')
                return

    def process_one_task(self):
        loaded_task = None
        prospective_state = State.fail
        try:
            try:
                loaded_task = self.acquire_task()
            except Missing:
                raise
            except Panic:
                raise
            except Exception as err:
                print(traceback.format_exc())
                print(f'Failed in initialization: {err}')
                # Log exception somehow.
                return

            starttime = datetime.now()
            self.log(f'Running {loaded_task.name} ...')
            exception = None
            try:
                loaded_task.run(self)
            except Exception as err:
                # Log the exception somehow
                stacktrace = traceback.format_exc()
                print(stacktrace)
                fname = f'stacktrace.rank{self.comm.rank:02d}.err'
                stacktracefile = loaded_task.entry.directory / fname
                stacktracefile.write_text(stacktrace)
                exception = err
                self.log(
                    f'Task {loaded_task.name} failed: {exception_summary}')
            else:
                prospective_state = State.done
                endtime = datetime.now()
                elapsed = endtime - starttime
                self.log(f'Task {loaded_task.name} finished in {elapsed}')

        finally:
            if loaded_task is not None and self.comm.rank == 0:
                loaded_task.update_state(
                    self.cache, prospective_state, self.myqueue_id,
                    self.name,
                    exception=exception_summary(exception))
