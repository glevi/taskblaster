from functools import wraps
import os
from pathlib import Path
import sys

import click

from taskblaster.parallel import choose_subworkers
from taskblaster.repository import Repository
from taskblaster.state import State


def silent_option():
    return click.option('-s', '--silent', is_flag=True,
                        help='Do not print to screen.')


def dryrun_option():
    return click.option('-z', '--dry-run', is_flag=True,
                        help='simulate what would happen, but do nothing')


def max_tasks_option():
    return click.option('--max-tasks', type=int, default=-1,
                        metavar='NUM',
                        help='Maximum number of tasks for worker to run')


def tree_argument():
    return click.argument(
        'tree',
        type=click.Path(exists=True, dir_okay=True,
                        file_okay=False, readable=True),
        nargs=-1)


def format_node_short(node):
    from taskblaster.util import color
    state = node.state
    return ' '.join([
        color(node.digest.short.center(8), state.color),
        color(state.name, state.color).ljust(17),
        color(node.name)])


def _repository(directory=None):
    if directory is None:
        directory = Path.cwd()

    try:
        return Repository.find(directory)
    except Repository.RepositoryError as ex:
        raise click.ClickException(
            f'{ex}.  '
            'Run tb init to initialize empty repository here.')


def with_repo(func):

    @wraps(func)
    def wrapper(*args, **kwargs):
        with _repository() as repo:
            return func(*args, **kwargs, repo=repo)

    return wrapper


@click.group()
def tb():
    """Taskblaster, a high-throughput workflow utility.

    Utility to define and run vast quantities of computational tasks,
    organizing inputs and outputs in a directory tree.

    Use the init subcommand to initialize an empty repository.  Then
    write a workflow function and run it to create a directory tree of
    tasks.  Then submit tasks using your favourite high-performance
    computing batch system.
    """


@tb.command()
@click.argument('script', type=click.Path(exists=True))
@dryrun_option()
@silent_option()
@with_repo
def workflow(repo, script, dry_run, silent):
    """Run workflow creating folders for tasks inside tree."""
    repo.run_workflow_script(script, dry_run, silent)


@tb.command()
@click.argument('pymodule', default='taskblaster.repository',
                metavar='[MODULE]')
@click.argument('directory', default='.')
def init(pymodule, directory):
    """Initialize repository inside directory."""
    root = Path(directory).resolve()
    try:
        Repository.create(root, modulename=pymodule)
    except Repository.RepositoryError as ex:
        raise click.ClickException(ex)
    print(f'Created repository using module "{pymodule}" in "{root}"')


@tb.command()
@with_repo
def info(repo):
    """Print info on repository."""
    info = repo.info()
    print('\n'.join(info))


class SubmissionImpossible(Exception):
    pass


def new_mq_task(future, ancestors, run_module, resources):
    deps = []

    for parent_name in future.node.parents:
        parent, parent_state, parent_mq_task = ancestors[parent_name]

        if parent_state.unsuccessful:
            raise SubmissionImpossible('Parent not successful')

        # If all ancestors were successful, the parents would have
        # been submitted.  If parent is still new, it indicates that
        # a grandparent somewhere could not be submitted.
        if parent_state == State.new:
            raise SubmissionImpossible('Ancestors not successful')

        assert (parent_state == State.done
                or parent_state.handled_by_myqueue)

        if parent_mq_task is not None:
            deps.append(parent_mq_task)

    if resources is None:
        resources = future._entry.resources.read()

    from myqueue.task import task as _mq_task

    mq_task = _mq_task(
        cmd=run_module,
        name=future.key.replace('/', '_'),
        args=[],
        deps=deps,
        folder=str(future.directory),
        resources=resources)

    return mq_task


def choose_states(ctx, param, value: str):
    # (None means any state)
    if value is None:
        return None

    choice = set(value)
    statecodes = State.statecodes()

    bad_states = choice - set(statecodes)

    if bad_states:
        raise click.BadParameter(
            'States must be among {}; got {}'.format(
                statecodes, ''.join(bad_states)))

    return {State(value) for value in choice}


def state_option():
    return click.option(
        '--state', '-s', type=click.UNPROCESSED,
        callback=choose_states,
        help=('select only tasks with this state.  State be any of: {}.'
              .format(repr(State.statecodes()))))


@tb.command()
@tree_argument()
@click.option(
    '--parents', is_flag=True,
    help='List ancestors of selected tasks outside selection; '
    '(Default: False)',
    default=False)
@click.option(
    '--alphabetical', is_flag=True,
    help='Use alphabetical instead of topological ordering; '
    'Can be faster for large trees. (Default: False)',
    default=False)
@click.option(
    '--columns', '-c', is_flag=False,
    help='Columns to display: '
    'd: digest, '
    's: state, '
    'i: dependencies, '
    'f: folder, '
    'I: myqueue id and subworker, '
    't: time info, '
    'T: time duration, '
    'c: conflict, '
    'C: conflict info, '
    '(Default: siTIf)',
    default='siTIf')
@state_option()
@click.option(
        '--name', '-n',
        help='select only tasks with this name.')
@click.option(
        '--pattern', '-p',
        help='select only tasks with a folder that contains this string')
@with_repo
def ls(repo, tree, columns, state, parents, alphabetical, name, pattern):
    """List tasks under directory TREEs.

    Find tasks inside specified TREEs and collect their dependencies
    whether inside TREE or not.  Then perform the specified actions
    on those tasks and their dependencies."""
    repo.registry.workers.sync(repo)
    for line in repo.tree(tree, states=state).ls(parents=parents,
                                                 columns=columns):
        click.echo(line)


@tb.command()
@tree_argument()
@with_repo
def stat(repo, tree):
    """Print statistics about selected tasks."""

    # If patterns point to directories, we must recurse into
    # those directories, i.e. <pattern>/*.
    #
    # But we can't just append /* because then we don't match the
    # directory itself.
    #
    # We also can't append * because then we match more things
    # than the user wanted.

    # Here we're doing O(N) work which is not necessary
    # when we're only counting.
    repo.tree(tree).stat()


@tb.command()
@tree_argument()
@with_repo
def submit(repo, tree):
    """Mark tasks in TREE and dependencies for execution."""
    for future in repo.submit(tree):
        print(future.describe())


def setup_kill_signal_handlers():
    import signal

    def raise_systemexit(sig, frame):
        raise SystemExit(f'Interrupted by signal {sig}')

    for sig in [signal.SIGCONT, signal.SIGTERM]:
        signal.signal(sig, raise_systemexit)


@tb.command()
@tree_argument()
@click.option('--subworker-count', type=int,
              help='number of MPI subworkers in run')
@click.option('--subworker-size', type=int,
              help='number of processes in each MPI subworker')
@max_tasks_option()
def run(tree, subworker_count, subworker_size, max_tasks):
    """Launch worker to execute submitted tasks."""
    repo = _repository()

    # Should we queue any selected tasks or only the queued subset?
    # Maybe queue them unless given an option.
    #
    # So: We take tree as an input.  If user used glob patterns,
    # the shell will have expanded them already.  Thus,
    # we take some paths as an input.  They may be tasks
    # or they may have subfolders that are tasks.
    #
    # What we need is to select all subfolders.  It would appear that
    # we can use sqlite's glob functionality for this.
    #
    # Then we need to "submit" those, and then launch a worker selecting
    # only those.
    #
    # So actually, if we received 1000 dirs, we can't just hog them
    # right away.  We could submit them immediately, but then the
    # worker just needs to be able to not return anything *except*
    # something matching one of those 1000 dirs.

    # Workers can be killed in exotic ways; keyboardinterrupt,
    # SIGCONT, SIGTERM, SIGKILL, who knows.  We try to catch
    # the signals and finalize/unlock/etc. gracefully.
    setup_kill_signal_handlers()

    world = repo.mpi_world()
    subworker_size, subworker_count = choose_subworkers(
        size=world.size,
        subworker_count=subworker_count,
        subworker_size=subworker_size)

    comm = world.split(subworker_size)

    subworker_group = world.rank // subworker_size

    # TODO: Replace with MYQUEUE_ID when we have one:
    myqueue_id = os.getenv('SLURM_JOB_ID', 'N/A')

    subworker_id = f'{myqueue_id}-{subworker_group}/{subworker_count}'

    if tree:
        tree = repo.tree(tree, states={State.queue, State.new})

        def find_tasks():
            runnable = {State.queue, State.new}
            for indexnode in tree.nodes_topological():
                # The nodes_topological() iterator may have outdated
                # information, so we need to refresh everything we see:
                indexnode = repo.cache.registry.index.node(indexnode.name)

                if indexnode.state not in runnable:
                    continue

                if indexnode.awaitcount != 0:
                    continue

                yield indexnode

        selection = find_tasks()
    else:
        selection = None

    from taskblaster.worker import Worker
    worker = Worker(repo=repo, name=subworker_id, myqueue_id=myqueue_id,
                    comm=comm, selection=selection, max_tasks=max_tasks)
    worker.main()


@tb.command()
@tree_argument()
@with_repo
@state_option()
@click.option('--force', default=False,
              is_flag=True,
              help='Unrun tasks without prompting for confirmation.')
def unrun(tree, repo, state, force):
    """Delete output files from TREE and reset task state to new.

    Unrunning a task also unruns its descendants."""
    # Might be wise to concentrate actual outputs inside a subdir
    # and then rmtree that subdir, except we hate using rmtree because
    # it is kind of dangerous.
    if not tree:
        return

    nodes, unrun = repo.tree(tree, states=state).select_unrun()

    for node in nodes:
        print(format_node_short(node))

    ntasks = len(nodes)
    if not ntasks:
        print('No tasks selected.')
        return

    if ntasks == 1:
        prompt = 'Unrun the above task?'
    else:
        prompt = f'Unrun the above {ntasks} tasks?'

    if force or click.confirm(click.style(prompt, 'bright_red')):
        unrun()

        if ntasks == 1:
            print('One task was unrun')
        else:
            print(f'{ntasks} tasks were unrun')
    else:
        print('Never mind.')


@tb.command()
@tree_argument()
@with_repo
@state_option()
def resolve(tree, repo, state):
    """Mark conflict as resolved on TREE.

    Task which has conflict state = conflict will be update to resolved"""
    if not tree:
        return

    repo.tree(tree, states=state).resolve_conflict()


@tb.command()
@tree_argument()
@with_repo
@state_option()
def unresolve(tree, repo, state):
    """Undo 'tb resolve'
    Task which has conflict state = resolved will be updated to conflict
    state = conflict"""
    if not tree:
        return

    repo.tree(tree, states=state).unresolve_conflict()


@tb.command()
def completion():
    """Print command-line completion incantation.

    To enable command-line completion, include the
    script the shell rc file, e.g., ~/.bashrc."""
    import subprocess

    progname = Path(sys.argv[0]).name
    name = progname.replace('-', '_').upper()
    command = ('echo "$(_{}_COMPLETE=bash_source {})"'.
               format(name, progname))

    subprocess.run(command, shell=True, check=True)


@tb.command()
@tree_argument()
@dryrun_option()
@with_repo
@state_option()
def remove(tree, dry_run, repo, state):
    """Delete tasks entirely.  Caution is advised."""

    if not tree:
        return

    nodes, delete = repo.tree(tree, states=state).remove()

    msg = 'would delete:' if dry_run else 'deleting:'
    for node in nodes:
        print(msg, node.digest.short, node.name)

    ntasks = len(nodes)
    if not ntasks:
        print('No tasks selected')
        return

    prompt = (f'WARNING: This permanently removes the above task(s).\nAre '
              f'you certain about deleting the above {ntasks} task(s)?')
    if not dry_run and click.confirm(click.style(prompt, 'bright_red')):
        delete()
    elif not dry_run:
        print('Never mind.')


@tb.command()
@with_repo
@click.option('--action', type=str,
              help='perform this action for the selected tasks.')
@tree_argument()
def view(repo, action, tree):
    """View detailed information or execute task-specific commands."""
    repo.view(tree, action=action)


@tb.command()
@tree_argument()
@with_repo
def graph(repo, tree):
    """Generate dependency graph."""
    repo.graph(tree)


@tb.command()
@click.argument('flag')
@tree_argument()
@click.option('-d', '--delete', is_flag=True, default=False,
              help='delete flag')
@with_repo
def flag(repo, tree, flag, delete):
    """Add or delete FLAGs in TREE.

    Apply the specified FLAG to entries in TREE.
    With -d, remove FLAG instead of applying it."""
    tree = repo.oldtree(tree)
    for entry in tree.entries():
        flags = entry.flags.read()

        if delete:
            if flag in flags:
                print(f'Unflagged {entry} as {flag}')
                flags.remove(flag)
                entry.flags.write(flags)
        else:
            if flag not in flags:
                print(f'Flagged {entry} as {flag}')
                flags.add(flag)
                entry.flags.write(flags)


# We need to return whichever thing ASR needs/wants.
# This includes the with_repo decorator.
# We should probably return an object of some particular type instead.
# return cli, with_repo


def conflict_error(err):
    msg = """\
A task already exists in this directory with different inputs.
You may wish to assign a different name for the task in the workflow,
or delete the old task."""

    # We need also some error handling options, e.g., skip conflicts,
    # or always override, or choosing interactively.
    # Even better, we could generate multiple conflicts and list them
    # at the end.
    return click.ClickException(f'{err}\n{msg}')


def define_subgroups():
    from taskblaster.cli_registry import registry
    from taskblaster.cli_workers import workers

    tb.add_command(registry)
    tb.add_command(workers)


define_subgroups()


# tb_registry = define_registry_subcommand(tb)
cli = tb  # Old alias referenced by pip installations


if __name__ == '__main__':
    tb.main()
