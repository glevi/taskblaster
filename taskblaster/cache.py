from pathlib import Path
from collections.abc import Mapping
from tempfile import mkdtemp

from taskblaster.future import Future
from taskblaster.entry import Entry


class FileCache(Mapping):
    def __init__(self, *, directory, registry, json_protocol):
        self.directory = Path(directory)
        self._absdir = self.directory.absolute()

        self.registry = registry
        self.json_protocol = json_protocol

    def entry(self, name) -> Entry:
        if not self.registry.contains(name):
            raise KeyError(name)
        directory = self.directory / name
        return Entry(directory, self.json_protocol)

    def __len__(self):
        return self.registry.index.count()

    def __iter__(self):
        return (regnode.name for regnode in self.registry.index.nodes())

    def __getitem__(self, name) -> Future:
        entry = self.entry(name)
        node = entry.node(name)
        return Future(node, self)

    def __repr__(self):
        return (f'{type(self).__name__}({self.directory}, '
                f'[{self.registry.index.count()} entries])')

    def _make_tempdir(self, subdirpath, name):
        directory = self.directory / subdirpath
        directory.mkdir(exist_ok=True, parents=True)
        return Path(mkdtemp(prefix=f'{name}-', dir=directory))

    def _store_node(self, node, taskdir, exist_ok=False):
        Entry.create(node, taskdir, self.json_protocol, exist_ok=exist_ok)

    def add_or_update(self, node, worker=None, force_overwrite=False):
        from taskblaster.state import State
        if self.registry.contains(node.name):
            previous_node = self.entry(node.name).node(node.name)
            previous_indexnode = self.registry.index.node(node.name)

            if node != previous_node:

                if previous_indexnode.state != State.new:
                    # We could punt the state back to new and overwrite,
                    # but for now the user will have to unrun manually.

                    con, info, buf = self.registry.get_conflict_info(
                        previous_indexnode.name)

                    new_buf = node._buf
                    if buf != new_buf:
                        self.registry.update_conflicts(
                            previous_indexnode.name,
                            conflict='c',
                            reason=previous_node._buf,
                            buf=new_buf)

                        return 'conflict', previous_indexnode
                    return 'resolved', previous_indexnode

                # Maybe we should only overwrite if previous node is 'new'
                #  - if it's failed, we probably shouldn't delete errmsg etc.
                #  - what if it's queued?
                force_overwrite = True
            # check if conflict has been resolved and update conflicts
            self.registry.clear_conflict(previous_indexnode.name)
        action, indexnode = self.registry.add_or_update(
            node, worker, force_overwrite=force_overwrite)

        assert action in {'add', 'update', 'have'}

        # We re-construct the taskdir from the indexnode
        # since it could be pointing to same hash under different dir:
        taskdir = self._absdir / indexnode.name

        # Not super pretty that we are doing non-transactional file ops
        # in the middle of a transaction.  What is a reasonable way
        # to make this more correct?
        if action == 'add':
            taskdir.mkdir(parents=True)

        if action in {'add', 'update'}:
            self._store_node(node, taskdir, exist_ok=action == 'update')

        return action, indexnode

    def dir2name(self, directory):
        return str(directory.relative_to(self.directory))

    def find_ready(self):
        """Return a task which is ready to run right now.

        Returns None if no such task is found.
        """

        # TODO: Change to raise NoSuchTask() if there is no task.
        # Also: We need to find things only with the right kind of worker.
        # What if we depend on things in other directories?  We need to
        # be able to run those, too.
        #
        # Probably we should also be able to limit searching to a particular
        # directory, but that doesn't always work well since dependencies
        # often reside in different directories
        return self.registry.find_ready()

    def update_state(self, entry, state):
        name = self.dir2name(entry.directory)
        self.registry.update_state(name, state)

    def delete_nodes(self, nodes):
        # XXX must work if registry/tree are inconsistent.
        import shutil
        entries = [self.entry(node.name) for node in nodes]
        cachedir = self._absdir.resolve()

        for entry in entries:
            assert cachedir in entry.directory.parents

        for entry in entries:
            entry.delete()

            directory = entry.directory.resolve()
            # Let's be a little bit paranoid before rmtreeing:
            assert 'tree' in directory.parent.parts
            assert self.directory.is_absolute()
            assert self.directory in directory.parents
            if directory.exists():
                shutil.rmtree(directory)
            remove_empty_dirs(cachedir, entry.directory)

        self.registry.remove_nodes(nodes)


def remove_empty_dirs(root, directory):
    """Remove directory and all empty parent directories up to root."""
    if root not in directory.parents:
        raise RuntimeError(f'Refusing to delete dirs outside {root}!')

    for parent in directory.parents:
        if parent == root:
            break
        try:
            parent.rmdir()
        except OSError:  # directory not empty
            break


class DirectoryConflict(OSError):
    pass
