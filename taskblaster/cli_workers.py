import click

import taskblaster.cli as cli


@click.group()
def workers():  # noqa: F811
    """View or manipulate workers."""


@workers.command()
@cli.with_repo
def ls(repo):
    """List taskblaster workers."""
    registry = repo.cache.registry
    registry.workers.sync(repo)
    registry.workers.ls()


@workers.command()
@cli.with_repo
@cli.dryrun_option()
@click.option('-j', '--count', default=1, type=int,
              metavar='NUM',
              help='how many workers to submit')
@click.option('--subworker-count', default=None, type=int,
              help='number of MPI subworkers in run')
@click.option('--subworker-size', default=None, type=int,
              help='number of processes in each MPI subworker')
@click.option('-R', '--resources',
              help='resource specification forwarded to myqueue')
@cli.max_tasks_option()
def submit(repo, dry_run, count, resources, max_tasks,
           subworker_count, subworker_size):
    """Submit one or more workers as myqueue jobs."""
    registry = repo.cache.registry
    registry.workers.submit_workers(repo, dry_run, count, resources, max_tasks,
                                    subworker_count, subworker_size)
