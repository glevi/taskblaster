from pathlib import Path

from taskblaster.state import State
from taskblaster.util import color


def normalize_patterns(repo, directories):
    if not directories:
        directories = ['.']

    # XXX cwd
    cwd = Path.cwd()

    def patternize(pattern):
        path = cwd / pattern
        if path == repo.root:
            path = repo.cache.directory

        # XXX do error checking in some kind of preprocessing step
        # try:
        relpath = path.relative_to(repo.cache.directory)
        # except ValueError:
        #    raise click.ClickException(f'"{pattern}" is outside tree')

        pattern = str(relpath)
        if pattern == '.':
            pattern = '*'  # XXX not very logical
        return pattern

    return [patternize(directory) for directory in directories]


class Tree:
    def __init__(self, repo, directories, states=None):
        self.repo = repo
        # Some of these are actually patterns that we use to glob
        # inside the registry, which is a bit misleading.
        self.directories = normalize_patterns(repo, directories)

        self.registry = repo.registry
        self.cache = repo.cache

        self.states = states

    def nodes(self, name=None, pattern=None):
        return self._nodes(False, set())

    def nodes_topological(self, reverse=False, parents=False):
        return self._nodes(topological_order=True, seen=set(), reverse=reverse)

    def _recurse_kin(self, node, seen, reverse=False):
        if node.name in seen:
            return

        ancestry = self.registry.ancestry
        if reverse:
            kin_names = ancestry.descendants(node.name)
        else:
            kin_names = ancestry.ancestors(node.name)

        for kin_name in kin_names:
            if kin_name in seen:
                continue
            kin = self.registry.index.node(kin_name)
            yield from self._recurse_kin(kin, seen, reverse=reverse)

        seen.add(node.name)
        yield node

    def submit(self):
        # XXX We need only loop recursively over nodes that are new.
        nodes = [node for node in self.nodes_topological()
                 if State(node.state) == State.new]
        self.registry.index.update_states([node.name for node in nodes],
                                          State.queue.value)
        return nodes

    def _update_conflict(self, oldstate, newstate):
        nodes = [node for node in self.nodes()
                 if node.state != State.new]
        for node in nodes:
            task_name = node.name
            conflict, info, inp = self.registry.get_conflict_info(task_name)
            if conflict == oldstate:
                self.registry.update_conflicts(task_name, conflict=newstate,
                                               reason=info, buf=inp)

    def resolve_conflict(self):
        self._update_conflict('c', 'r')

    def unresolve_conflict(self):
        self._update_conflict('r', 'c')

    def select_unrun(self):
        nodes = [node for node in self.nodes_topological(reverse=True)
                 if node.state not in {State.new, State.cancel}]

        def unrun():
            for node in nodes:
                entry = self.cache.entry(node.name)

                self.registry.unrun(node.name)
                if node.state == State.done:
                    outputfile = entry.outputfile
                    outputfile.unlink(missing_ok=True)
                    # Should nuke the whole directory

                self.registry.index.update_digest(node.name, '')
            return len(nodes)

        return nodes, unrun

    def remove(self):
        nodes = list(self.nodes_topological(reverse=True))

        def delete():
            self.cache.delete_nodes(nodes)
            print(f'{len(nodes)} task(s) were deleted.')

        return nodes, delete

    def _nodes(
            self, topological_order, seen, reverse=False,
            # parents=False, name=None, pattern=None
    ):
        for directory in self.directories:
            nodes = self.registry.index.glob([directory], states=self.states)

            for node in nodes:
                if node.name in seen:
                    continue

                if topological_order:
                    yield from self._recurse_kin(node, seen, reverse=reverse)
                else:
                    seen.add(node.name)
                    yield node

    def ls(self, parents=False, columns='dsifIt'):
        # each element is either a task or a subdirectory to recurse.
        if parents:
            iternodes = self.nodes_topological(parents=parents)
        else:
            iternodes = self.nodes()

        cwd = Path.cwd()

        info_dct = {'d': 'digest'.ljust(11),
                    's': 'state'.ljust(8),
                    'i': 'deps'.ljust(6),
                    'f': 'folder'.ljust(39),
                    'I': 'mq id-sw'.ljust(13),
                    't': 'start time'.ljust(15) + 'end time'.ljust(16),
                    'T': 'time'.ljust(13),
                    'c': 'conflict'.ljust(11),
                    'C': 'conflict info'.ljust(15)}

        headerline = ''
        for key in columns:
            headerline += info_dct[key]
        yield headerline
        yield '─' * len(headerline)

        for node in iternodes:
            yield format_node(self.registry, node,
                              treedir=self.cache.directory,
                              fromdir=cwd, columns=columns)

    def stat(self):
        nodes = {}

        for node in self.nodes():
            nodes[node.name] = node

        counts = {state: 0 for state in State}

        for node in nodes.values():
            counts[node.state] += 1

        for state in State:
            num = counts[state]
            # The strings are long because of the ANSI codes.
            # Could/should we be able to get the printed string length
            # somehow?
            print(f'{state.ansiname:18s} {num}')

        return counts


def format_node(registry, node, columns='dsif', *, treedir, fromdir):
    import datetime
    from taskblaster.future import parent_state_info
    from taskblaster.registry import UNKNOWN_AWAITCOUNT
    from taskblaster.state import ConflictState

    state = State(node.state)
    okcount, nparents, deps_color = parent_state_info(
        registry, node.name)

    if node.awaitcount == UNKNOWN_AWAITCOUNT:
        nparents = '?'

    directory = treedir / node.name
    relpath = directory.relative_to(fromdir)

    worker_name, start_time, end_time, exception = registry.get_runlog_info(
        node.name)

    conflict, conflictinfo, conflictinp = registry.get_conflict_info(node.name)
    conflictinfo = str(conflictinfo)[1:]
    if conflict:
        conflict = ConflictState(conflict)
    else:
        conflict = ConflictState.none

    if end_time is not None:
        duration = end_time - start_time
    elif start_time is not None:
        duration = datetime.datetime.now() - start_time
    else:
        duration = None
    if duration is not None:
        days, seconds = duration.days, duration.seconds
        hours = seconds // 3600
        minutes = (seconds % 3600) // 60
        seconds = seconds = seconds % 60
        if days > 0:
            duration = f'{days}d {hours:02}:{minutes:02}:{seconds:02}'
        else:
            duration = f'   {hours:02}:{minutes:02}:{seconds:02}'
    else:
        duration = ''

    def format_timestamp(timestamp) -> str:
        if timestamp is None:
            return ''
        return timestamp.strftime('%y-%m-%d %H:%M')

    start_time = format_timestamp(start_time)
    end_time = format_timestamp(end_time)

    folder_name = str(relpath)
    info_dct = {
        'd': color(node.digest.short.center(8).ljust(10), state.color),
        's': color(state.name.ljust(7), state.color),
        'i': color(f'{okcount}/{nparents}'.ljust(5), deps_color),
        'f': color(folder_name.ljust(38), 'bright_green'),
        'I': worker_name.ljust(12) if worker_name != '' else ''.ljust(12),
        't': (start_time.ljust(15) + end_time.ljust(15)
              if worker_name != '' else ''.ljust(15)),
        'T': str(duration).ljust(12),
        'c': color(conflict.name.ljust(10), conflict.color),
        'C': conflictinfo.ljust(14)}

    output = []
    for key in columns:
        if key == 'f' and columns[-1] != key:
            if len(str(relpath)) > 38:
                folder_name = folder_name[:35] + "…"
            folder_name = folder_name.ljust(38)
            info_dct[key] = color(folder_name, 'bright_green')
        output.append(info_dct[key])
    line = ' '.join(output).rstrip()
    if exception is not None:
        exception = (exception[:120] + '…'
                     if len(exception) > 120 else exception)
        line += '\n^^^^  ' + color(exception, 'bright_red')
    return line
