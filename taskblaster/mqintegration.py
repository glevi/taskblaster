class BadDependency(Exception):
    def __init__(self, digest):
        self.digest = digest


def create_mq_task(mqtasks, future, run_module, resources=None):
    from myqueue.task import task as mq_task

    deps = []
    for digest in future.node.parents:
        try:
            dep = mqtasks._dct[digest]
        except KeyError:
            raise BadDependency(digest)
        else:
            deps.append(dep)

    if run_module is None:
        run_module = 'taskblaster.run'

    task_kwargs = dict(
        cmd=run_module,
        name=future.key.replace('/', '_'),
        args=[],
        deps=deps,
        folder=str(future.directory))

    if not resources:
        resources = future._entry.resources.read()

    if resources:
        task_kwargs.update(resources=resources)

    task = mq_task(**task_kwargs)
    return task


class MQTasks:
    def __init__(self, workdir, run_module, resources=None):
        self.run_module = run_module
        self.tasks = []
        self._dct = {}
        self.workdir = workdir
        self.resources = resources

    def add(self, future):
        # In myqueue, we choose what to submit based on which directory
        # we are in.
        if self.workdir not in future.directory.parents:
            return None

        task = create_mq_task(self, future, resources=self.resources,
                              run_module=self.run_module)
        self.tasks.append(task)
        self._dct[future.node.digest] = task
        return task


def myqueue(dry_run=False):
    from myqueue.config import Configuration
    from myqueue.queue import Queue
    # XXX depends on pwd does it not?  Should probably depend on workflow
    # location.
    config = Configuration.read()
    return Queue(config=config, dry_run=dry_run)


def submit_manytasks(tasks, dry_run, max_mq_tasks=None):
    from myqueue.submitting import submit

    with myqueue(dry_run=dry_run) as queue:
        submit(queue, tasks, max_tasks=max_mq_tasks)


def mq_worker_task(directory, resources, worker_module, max_tasks=None,
                   subworker_size=None, subworker_count=None):
    from myqueue.task import create_task

    args = ['run']

    if max_tasks is not None:
        args.append(f'--max-tasks={max_tasks:d}')
    if subworker_size is not None:
        args.append(f'--subworker-size={subworker_size:d}')
    if subworker_count is not None:
        args.append(f'--subworker-count={subworker_count:d}')

    return create_task(
        cmd='taskblaster',
        name='worker',
        args=args,
        deps=[],
        resources=resources,
        # processes=1,  # XXX
        folder=directory)
