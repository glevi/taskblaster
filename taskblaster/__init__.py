from typing import Any, Dict
from abc import ABC, abstractmethod
import functools


class HashedExternalFile:
    def __init__(self, path, digest):
        self.path = path
        self.digest = digest

    def __repr__(self):
        return f'HashedExternalFile({self.path} [{self.digest}])'

    def tb_encode(self):
        return dict(path=str(self.path), digest=self.digest)

    @classmethod
    def tb_decode(cls, data):
        return cls(**data)


class ExternalFile:
    def __init__(self, path):
        from pathlib import Path
        # XXX should be resolved relative to root
        self.path = Path(path).resolve()

    def tb_encode(self):
        return str(self.path)

    @classmethod
    def tb_decode(cls, data):
        return cls(data)

    def _filehash(self):
        from hashlib import sha256
        chunksize = 2**16
        hashobj = sha256()
        with self.path.open('rb') as fd:
            while True:
                chunk = fd.read(chunksize)
                if not chunk:
                    break
                hashobj.update(chunk)
        return hashobj.hexdigest()

    def hashed(self):
        digest = self._filehash()
        return HashedExternalFile(self.path, digest)

    def __repr__(self):
        return f'ExternalFile({self.path!r})'


class InputVariable:
    def __init__(self, default):
        self._default = default


_no_default = object()


def var(default=_no_default):
    return InputVariable(default)


class StopWorkflow(Exception):
    pass


class TaskView:
    # This should provide access to both inputs and outputs
    # and any other information for the sake of user actions.
    def __init__(self, output):
        self.output = output


def actions(**actions):
    """Return decorator for task function to provide custom actions."""

    def deco(func):
        func._tb_actions = actions
        return func

    return deco


class subworkflow(property):
    def __init__(self, meth):
        self.meth = meth
        self._tb_subworkflow = True

        @functools.wraps(meth)
        def wrapper(self, **kwargs):
            workflow = meth(self, **kwargs)
            workflow._rn = self._rn.with_subdirectory(meth.__name__)
            return workflow

        super().__init__(fget=wrapper)


class TaskSpecification(property):
    def __init__(self, unbound_meth, worker, dynamical):
        self.unbound_meth = unbound_meth
        self.worker = worker
        self.dynamical = dynamical

        # magical property to tell runner that this object is "tasklike"
        self._tb_task = True

        super().__init__(fget=self._fget)

    @property
    def methname(self):
        return self.unbound_meth.__name__

    def _fget(self, workflow):
        name = workflow._rn.get_full_name(self.methname)
        return BoundTaskSpecification(name, self, workflow)

    @classmethod
    def decorator(cls, **kwargs):
        return lambda method: cls(method, **kwargs)

    def __repr__(self):
        return f'TaskSpecification({self.unbound_meth.__name__!r})'


class UserWorkflowError(Exception):
    pass


class BoundTaskSpecification:
    def __init__(self, name, declaration, workflow, index=tuple()):
        self.name = name
        self.declaration = declaration
        self.workflow = workflow
        self.index = index

    def __call__(self):
        """At workflow level, call wrapped unbound method to produce node."""
        try:
            return self.declaration.unbound_meth(self.workflow)
        except Exception as ex:
            raise UserWorkflowError from ex

    def _tb_pack(self):
        return {'__tb_type__': 'ref',
                'name': self.name,
                'index': self.index}

    def task(self):
        from taskblaster.namedtask import Task
        taskspec = self()
        return Task(self.name, taskspec.target, taskspec.kwargs,
                    dynamical=self.declaration.dynamical)

    def __repr__(self):
        return f'{type(self).__name__}({self.name!r})'

    def __getitem__(self, index):
        """Reference of index into output of this task specification.

        Return new reference which is an index into the return value of
        another task.

        For example a workflow can do tb.node(x=self.othertask['hello']).

        """
        newindex = (*self.index, index)
        return BoundTaskSpecification(
            self.name, self.declaration, self.workflow, newindex)


def task(_meth=None, *, worker=None, dynamical=False):
    deco = TaskSpecification.decorator(worker=worker, dynamical=dynamical)

    if _meth is None:
        return deco
    else:
        return deco(_meth)


class TaskSpec:
    def __init__(self, target, kwargs):
        self.target = target
        self.kwargs = kwargs

    def __repr__(self):
        return f'<TaskSpec({self.target}, {self.kwargs})>'


def node(target, **kwargs):
    return TaskSpec(target, kwargs)


def define(meth):
    @functools.wraps(meth)
    def wrapper(self):
        obj = meth(self)
        return node('define', obj=obj)
    return task(wrapper)


class Input:
    def __init__(self, value, *, _index=tuple()):
        # If the input variable has no default, it actually defaults to
        # the unique _no_default object which means we don't allow it.
        assert value is not _no_default
        self._value = value
        self.index = _index

    def __repr__(self):
        indextext = ''.join(f'[{part!r}]' for part in self.index)
        return f'<Input({self._value!r}){indextext}>'

    def _tb_pack(self):
        return self.getvalue()

    def getvalue(self):
        # XXX This indexing pattern exists in approximately two places.
        obj = self._value
        for index in self.index:
            obj = obj[index]
        return obj

    def __getitem__(self, item):
        return Input(self._value, _index=(*self.index, item))


def totree(definitions, name, root=None):

    def workflow(rn):
        if root is not None:
            rn = rn.with_subdirectory(root)

        for key in definitions:
            obj = definitions[key]
            rn1 = rn.with_subdirectory(key)
            fullname = f'{key}/{name}'
            rn1.define(obj, fullname)

    return workflow


class DummyUnboundTask:
    def __init__(self, future, *, _index=tuple()):
        self.future = future
        self.name = str(future.directory.relative_to(
            future._cache.directory))

        self.index = _index

    def peek(self):
        return self.future.peek()

    def _refhook(self):
        return self

    def _tb_pack(self):
        return {'__tb_type__': 'ref',
                'name': self.future.node.name,
                'index': self.index}

    def __getitem__(self, index):
        return DummyUnboundTask(self.future, _index=(*self.index, index))


def parametrize_glob(pattern):
    def wrap(workflow_cls):
        def workflow(rn):
            cache = rn._cache

            assert rn.directory.samefile(cache.directory)

            # XXX This should be a kind of query instead of
            # looping over all the values.
            for future in cache.values():
                # TODO: Efficient lookup for matching patterns.
                # Maybe we cannot do general globs then?  But that's okay.
                path = future.directory
                relpath = path.relative_to(cache.directory)

                if not relpath.match(pattern):
                    continue

                unbound_task = DummyUnboundTask(future)

                actual_workflow = workflow_cls(unbound_task)
                rn1 = rn.with_subdirectory(relpath.parent)
                rn1.run_workflow(actual_workflow)

        return workflow
    return wrap


def define_taskmeth(name, node):
    def taskmeth(self):
        return node
    taskmeth.__name__ = name
    return task(taskmeth)


def adhoc_workflow(meth):
    # Ugly hack where we dynamically generate a workflows class.
    # This goes the wrong way: We should make a workflow object that
    # isn't a class, such we can generate them dynamically
    import functools

    @functools.wraps(meth)
    def wrapper(self):
        class AdhocWorkflow:
            pass

        for name, node in meth(self):

            taskmeth = define_taskmeth(name, node)
            setattr(AdhocWorkflow, name, taskmeth)

        return workflow(AdhocWorkflow)()

    return subworkflow(wrapper)


class parametrize:
    _is_parametrize = True

    def __init__(self, taskname, keyword, unbound_task):
        self.taskname = taskname
        self.keyword = keyword
        self.unbound_task = unbound_task


def workflow(cls):
    inputvars = {}
    unbound_tasks = {}
    subworkflows = {}

    for name, value in vars(cls).items():
        if isinstance(value, InputVariable):
            inputvars[name] = value
        elif isinstance(value, TaskSpecification):
            unbound_tasks[name] = value
        elif isinstance(value, subworkflow):
            subworkflows[name] = value

    def constructor(self, **kwargs):
        self._accessed_attrs = None
        self._rn = None

        names = set(inputvars)

        for name in names:
            if name in kwargs:
                value = kwargs[name]
            else:
                value = inputvars[name]._default
                if value is _no_default:
                    raise TypeError(
                        f'Workflow missing required keyword argument: {name}')

            if not getattr(value, '_is_tb_workflow', None):
                # We want all workflow inputs to be "future"-ish.
                # Therefore:
                #  * Given a "future"-ish value, we do nothing in particular.
                #  * Given a concrete value, we store Input(value).
                value = Input(value)

            setattr(self, name, value)

    cls._is_tb_workflow = True
    cls.__init__ = constructor
    cls._inputvars = inputvars
    cls._unbound_tasks = unbound_tasks
    cls._subworkflows = subworkflows

    cls._inputmap = {v: k for k, v in inputvars.items()}

    def __repr__(self):
        clsname = type(self).__name__
        vartext = ', '.join(
            '{}={}'.format(varname, getattr(self, varname))
            for varname in sorted(self._inputvars))
        return f'<{clsname}({vartext})>'

    cls.__repr__ = __repr__

    def tb_encode(self):
        return {name: getattr(self, name) for name in self._inputvars}

    @classmethod
    def tb_decode(cls, data):
        assert set(data) == set(inputvars)
        return cls(**data)

    cls.tb_encode = tb_encode
    cls.tb_decode = tb_decode

    return cls


class JSONCodec(ABC):
    @abstractmethod
    def decode(self, dct: Dict[str, Any]) -> Any:
        ...

    @abstractmethod
    def encode(self, obj: Any) -> Dict[str, Any]:
        ...


def mpi(func):
    func._tb_mpi = True

    @functools.wraps(func)
    def wrapper(*args, mpi=None, **kwargs):
        """Initialize MPI communicators and call the decorated function."""
        # (We should probably use a shortcut for building the task context.)
        from taskblaster.repository import Repository
        from taskblaster.worker import TaskContext
        repo = Repository.find()
        mpi_world = repo.mpi_world()
        if mpi is None:
            mpi = TaskContext(mpi_world.usercomm(), mpi_world, 'main')
        return func(*args, mpi=mpi, **kwargs)

    return wrapper
