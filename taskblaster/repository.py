from pathlib import Path
import runpy
from typing import Any, Dict, List
import importlib

import taskblaster as tb
from taskblaster.cache import FileCache
from taskblaster.tree import Tree
from taskblaster.registry import Registry
from taskblaster.storage import JSONProtocol
from taskblaster.state import State
from taskblaster.runner import Runner


class Repository:
    _tasks_module_name = 'tasks.py'
    _tree_name = 'tree'
    _registry_name = 'registry.db'
    _magic_dirname = '.taskblaster'
    _py_filename = 'pymodule'

    class RepositoryError(Exception):
        pass

    # Other files:
    #  * lib/ -- task library directory (TODO)
    #  * tbconfig.ini -- configuration file or files (TODO)
    #  * ASR database specification file or files -- belongs in ASR though
    #
    # Also:
    #  * registry could be hidden if we have a tool for working with it.

    def __init__(self, root, usercodec=None, run_module='taskblaster.worker'):
        self.root = root.resolve()
        self.registry = Registry(self.registry_path)
        self.cache = FileCache(directory=self.tree_path,
                               registry=self.registry,
                               json_protocol=JSONProtocol(usercodec))
        self.run_module = run_module

        # When testing within a single process, it is useful to have a
        # namespace of ready-made tasks without having to import from files.
        self._tasks = {}

        from taskblaster.runner import define
        self._tasks['define'] = define

        self.usermodules = {}  # self.import_userscripts()

    def import_userscripts(self):
        """Import all the .py files under self.root.

        Python will then see them as modules, which means classes
        can be serialized/deserialized using module names with JSON/pickle."""
        import pkgutil

        usermodules = {}
        for module_info in pkgutil.walk_packages([str(self.root)]):
            scriptname, module = self.import_userscript(module_info)
            usermodules[scriptname] = module
        return usermodules

    def import_userscript(self, module_info):
        import sys
        import importlib.util

        # We consider ad-hoc modules to live inside this fake package:
        userpath = 'taskblaster.userpath'

        scriptname = f'{module_info.name}.py'
        file_path = Path(module_info.module_finder.path) / scriptname

        # XXX If any of this goes wrong, we should simply skip the
        # file.  Some files may be edited by user and we wouldn't want
        # a syntax error in one of those files to prevent workers
        # from running.
        module_name = f'{userpath}.{module_info.name}'

        spec = importlib.util.spec_from_file_location(module_name, file_path)
        module = importlib.util.module_from_spec(spec)
        sys.modules[module_name] = module
        spec.loader.exec_module(module)
        return scriptname, module

    def mpi_world(self):
        from taskblaster.parallel import SerialCommunicator
        return SerialCommunicator()

    @property
    def magicdir(self):
        return self.root / self._magic_dirname

    @property
    def registry_path(self):
        return self.magicdir / self._registry_name

    @property
    def tree_path(self):
        return self.root / self._tree_name

    def __enter__(self):
        # A process (e.g. this process) is allowed to hold the lock more
        # than once.  This is useful because we can perform actions
        # that require locking without needing to check whether we are
        # already holding the lock.
        #
        # Thus, we keep a count of the number of times we have acquired
        # the lock.
        #
        # The lock is not thread safe, but it is safe wrt. other (external)
        # processes.
        self.registry.conn.__enter__()
        return self

    def __exit__(self, *args):
        self.registry.conn.__exit__(*args)

    def __repr__(self):
        return f'<Repository(root={self.root})>'

    def tree(self, directories=None, states=None):
        return Tree(self, directories, states=states)

    def import_task_function(self, taskname):
        tokens = taskname.rsplit('.', 1)

        if taskname in self._tasks:
            return self._tasks[taskname]

        if len(tokens) == 2:
            module, funcname = tokens
            module = importlib.import_module(module)
            func = getattr(module, funcname)
        else:
            namespace = self.import_tasks()
            func = namespace[taskname]

        return func

    def import_tasks(self) -> Dict[str, Any]:
        # Should allow whole package in <root>/lib/ from which to import
        #
        # We need some way of guaranteeing that tasks cannot invoke
        # malicious functions.
        #
        # Right now we allow loading from any function named via
        # import path, so that can execute anything.
        #
        # Maybe ASR can have a way to point to 'valid' things.
        try:
            # we do str() for mypy
            target = str(self.tasks_module)
            return runpy.run_path(target, run_name=target)
        except FileNotFoundError:
            return {}
        #    raise self.RepositoryError('No tasks defined.  Define tasks in '
        #                               f'{self.tasks_module}')

    @property
    def tasks_module(self) -> Path:
        return self.root / self._tasks_module_name

    @classmethod
    def create(cls, root, modulename='taskblaster.repository') -> 'Repository':
        root = root.resolve()

        def trouble(msg):
            raise cls.RepositoryError(msg)

        try:
            module = importlib.import_module(modulename)
        except ModuleNotFoundError:
            trouble(f'Specified module "{modulename}" must exist')

        try:
            tb_init_repo = module.tb_init_repo
        except AttributeError:
            trouble(
                f'Specified module "{modulename}" '
                'does not implement a tb_init_repo(root) '
                'function or class')

        magic_dir = root / cls._magic_dirname
        magic_dir.mkdir(parents=True, exist_ok=True)

        modulefile = magic_dir / cls._py_filename
        modulefile.write_text(f'{modulename}\n')

        registry = magic_dir / cls._registry_name

        if registry.exists():
            trouble(f'Already exists: {registry}')
        registry.touch()

        tree_path = root / cls._tree_name
        tree_path.mkdir(exist_ok=True)
        repo = tb_init_repo(root)
        return repo

    @classmethod
    def find_root_directory(cls, directory='.'):
        directory = Path(directory).resolve()
        for root in (directory, *directory.parents):
            registry_location = root / cls._magic_dirname / cls._registry_name
            if registry_location.exists():
                return root

        raise cls.RepositoryError(
            f'No registry found in {directory} or parents')

    @classmethod
    def find(cls, directory='.') -> 'Repository':
        root = cls.find_root_directory(directory)
        pymodulefile = root / cls._magic_dirname / cls._py_filename
        pymodulename = pymodulefile.read_text().strip()
        pymodule = importlib.import_module(pymodulename)
        tb_init_repo = pymodule.tb_init_repo
        repo = tb_init_repo(root)
        assert isinstance(repo, cls), (repo, cls)
        return repo

    def plugin_pymodule(self):
        return (self.magicdir / self._py_filename).read_text().strip()

    def info(self) -> List[str]:
        have_tasks = self.tasks_module.is_file()
        taskfile = str(self.tasks_module)

        if not have_tasks:
            taskfile += ' (not created)'

        index = self.cache.registry.index

        lenstring = "entry" if index.count() == 1 else "entries"

        pymodulename = self.plugin_pymodule()
        pymodule = importlib.import_module(pymodulename)

        return [
            f'Module:     {pymodulename}',
            f'Code:       {pymodule.__file__}',
            f'Root:       {self.root}',
            f'Tree:       {self.cache.directory}',
            f'Registry:   {self.registry_path}'
            f' ({index.count()} {lenstring})',
            f'Tasks:      {taskfile}']

    # XXXX for testing.
    # The CLI implements the "real" call to workflows, which we should
    # actually move here as a different method.
    def run_workflow_blocking(self, workflow, *args, **kwargs):
        rn = self.runner()

        with self:
            obj = workflow(rn, *args, **kwargs)

            for node in self.tree().nodes_topological():
                future = self.cache[node.name]
                if future.has_output():
                    continue
                future.run_blocking(self)

        return obj

    def runner(self, silent=False, dry_run=False):
        rn = Runner(self, directory=self.cache.directory, dry_run=dry_run,
                    silent=silent)
        return rn

    def submit(self, tree):
        for node in self.tree(tree).submit():
            yield self.cache[node.name]

    def graph(self, tree):
        from taskblaster.util import tree_to_graphviz_text
        tree = self.tree(tree)
        txt = tree_to_graphviz_text(tree)
        print(txt)

    def _view(self, tree):

        def printlines_indented(string, *, indent):
            for line in string.split('\n'):
                spaces = ' ' * indent
                print(f'{spaces}{line}')

        for node in self.tree(tree).nodes():
            entry = self.cache.entry(node.name)
            hashednode = entry.node(node.name)

            state = State(node.state)

            print(f'name: {node.name}')
            print(f'  location: {entry.directory}')
            print(f'  digest:   {node.digest.long}')
            print(f'  state:    {state.name}')
            print(f'  target:   {hashednode.target}(…)')
            print(f'  wait for: {node.awaitcount} dependencies')
            print()

            print('  parents:')
            if hashednode.parents:
                for parent in hashednode.parents:
                    print(f'    {parent}')
            else:
                print('    <task has no dependencies>')
            print()

            inputstring = hashednode.input_repr(maxlen=20)
            print('  input:')
            printlines_indented(inputstring, indent=4)
            print()

            print('  output:')
            if state == State.done:
                outputstring = repr(entry.output())
                printlines_indented(outputstring, indent=4)
            else:
                print('    <task not finished yet>')
            print()

            function = self.import_task_function(hashednode.target)
            actions = getattr(function, '_tb_actions', {})
            if actions:
                print('  actions:')
                for action, function in actions.items():
                    origin = (f'{function.__name__}() '
                              f'from [{function.__module__}]')
                    print(f'    {action}: {origin}')
                    if function.__doc__ is not None:
                        line = function.__doc__.strip().split('\n')[0]
                        print(f'      {line}')
            else:
                print('No custom actions defined for this task.')
            print()

    def view(self, tree, action=None):
        if action is None:
            return self._view(tree)
        else:
            return self._run_action(tree, action=action)

    def _run_action(self, tree, action):
        for node in self.tree(tree).nodes():
            entry = self.cache.entry(node.name)
            hashednode = entry.node(node.name)

            function = self.import_task_function(hashednode.target)
            actions = getattr(function, '_tb_actions', {})

            if action not in actions:
                print(f'<node "{node.name}" does not have action "{action}">')
                continue

            if not entry.has_output():
                print(f'<node "{node.name}" has no output, skipping>')
                continue

            actionfunc = actions[action]
            if entry.has_output():
                output = entry.output()
                record = tb.TaskView(output)
                actionfunc(record)

    def run_workflow_script(self, script, dry_run, silent):
        import runpy
        namespace = runpy.run_path(script)
        workflow = namespace['workflow']
        # workflow_module = self.usermodules[script]
        # workflow = workflow_module.workflow

    # def run_workflow(self, workflow, dry_run=False):
        # Should we use introspection for mapping arguments to parametrized
        # workflows?  I think not, since it leaves us unable to define
        # whether we want Record, output, or indexed output.
        #
        # Nevertheless here we go, until we define a better way.
        # Probably we should instead decorate.  Whatever we do, we will
        # likely want this statically defined (i.e. in the workflow code
        # somehow) and not do it "ephemerally" at CLI level.

        rn = self.runner(dry_run=dry_run, silent=silent)
        # try:
        # with self.registry.conn:
        workflow(rn)
        # except DirectoryConflict as err:
        # Task exists with same name but different hash, i.e.,
        # inputs have changed.
        # We should properly visualize the differences between
        # inputs.
        # raise conflict_error(err)
        # XXX re-enable proper errmsg
        # raise err


def tasks_from_namespace(namespace):
    tasks = {}
    for name, obj in namespace.items():
        if callable(obj):
            tasks[name] = obj
    return tasks


def tb_init_repo(root):
    return Repository(root)
