import hashlib


def mkhash(buf: bytes) -> str:
    return hashlib.sha256(buf).hexdigest()


class _Unhashed:
    short = '—'  # printable representation
    long = ''  # stored representation

    def __repr__(self):
        return 'Unhashed()'


unhashed = _Unhashed()


class Digest:
    def __init__(self, value):
        assert len(value) == 64, value
        self._value = value

    @classmethod
    def frombuf(cls, buf: bytes) -> str:
        return cls(mkhash(buf))

    def _tostring(self, maxlen=64):
        return self._value[:maxlen]

    @property
    def long(self) -> str:
        return self._tostring()

    @property
    def short(self) -> str:
        return self._tostring(8)

    def __repr__(self):
        return f'Digest({self.long})'

    def __str__(self):
        return f'Digest({self.short})'

    def __eq__(self, other):
        if not isinstance(other, Digest):
            return NotImplemented

        return self._value == other._value

    def __neq__(self, other):
        if not isinstance(other, Digest):
            return NotImplemented
        return not self == other
