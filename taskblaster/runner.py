from pathlib import Path
import traceback

from taskblaster import UserWorkflowError
from taskblaster.future import Future
from taskblaster.hashednode import Node
from taskblaster.namedtask import Task, new_workflowtask


def define(obj):
    return obj


def run_workflow(rn, workflow):
    assert workflow._rn is None
    workflow._rn = rn

    subworkflows = {name: getattr(workflow, name)
                    for name in workflow._subworkflows}
    tasks = {name: getattr(workflow, name)
             for name in workflow._unbound_tasks}

    repo = rn._repo
    assert repo.registry.index.conn._conn is None
    with repo:
        assert repo.registry.index.conn._conn is not None
        for name, subworkflow in subworkflows.items():
            xxxxxxxxxxxxxxxxx
            fullname = rn.get_full_name(name)
            workflowtask = new_workflowtask(subworkflow, fullname)
            assert repo.registry.index.conn._conn is not None

            rn._add_task(workflowtask)

        for name in workflow._unbound_tasks:
            unbound_task = getattr(workflow, name)
            task = unbound_task.task()
            rn._add_task(task)


def print_traceback(ex):
    print('--- Error in workflow ----------------')
    print(''.join(traceback.format_exception(ex)))
    print('--------------------------------------')


class Runner:
    def __init__(self, repo, *,
                 directory=Path(), dry_run=False, silent=False,
                 resources=''):
        self._repo = repo
        self._directory = Path(directory)
        self._resources = resources
        self._dry_run = dry_run
        self._silent = silent

    @property
    def prefix(self):
        return str(self._directory)

    @property
    def _cache(self):
        return self._repo.cache

    @property
    def directory(self):
        return self._repo.tree_path / self._directory

    def with_subdirectory(self, directory):
        return self._new(directory=self._directory / directory)

    def with_resources(self, resources: str):
        return self._new(resources=resources)

    def get_full_name(self, name):
        fullpath = self.directory / name
        relpath = fullpath.relative_to(self._cache.directory)
        return str(relpath)

    def _new(self, **kwargs):
        kwargs = {
            'repo': self._repo,
            'dry_run': self._dry_run,
            'silent': self._silent,
            'directory': self._directory,
            'resources': self._resources,
            **kwargs}

        return Runner(**kwargs)

    def _find_refs(self, task):
        import json
        from taskblaster import Input, BoundTaskSpecification

        class ReferenceFinder:
            def __init__(self, codec):
                self._refs = []
                self.codec = codec

            def default(self, obj):
                if isinstance(obj, Input):
                    return obj.getvalue()
                if isinstance(obj, BoundTaskSpecification):
                    self._refs.append(obj)
                    return None
                if isinstance(obj, Future):
                    # XXX also there's Reference
                    raise RuntimeError('should not receive Future here?')

                if hasattr(obj, '_refhook'):
                    ref = obj._refhook()
                    self._refs.append(ref)
                    return None

                return self.codec.encode(obj)

            def find_refs(self, task):
                # (The encoder extracts the references as a side
                # effect of encoding.)
                json.dumps(task.kwargs, default=self.default)
                return self._refs

        reffinder = ReferenceFinder(
            codec=self._cache.json_protocol.codec)
        refs = reffinder.find_refs(task)
        return refs

    def add_workflow(self, name, workflow):
        task = new_workflowtask(workflow, name)
        self._add_task(task)

    def run_workflow(self, workflow):
        seen = set()

        for target_task in self._iterate_tasks(workflow):
            for required_task in self._topological_order(target_task, seen):
                assert required_task.name in seen
                self._add_task(required_task)

    def _topological_order(self, task, seen):
        if task.name in seen:
            return

        refs = self._find_refs(task)
        for ref in refs:
            if ref.name in seen:
                continue

            if ref.name in self._cache:
                # Ignore task which already exists -- we don't need
                # to generate that task.
                #
                # Although somewhere we should check, or be able to check,
                # that it isn't outdated.
                continue

            try:
                ref_task = ref.task()
            except UserWorkflowError:
                # We have this exception check in two places,
                # maybe it can be better?
                print(f'{task.name} unreachable due to dependency '
                      f'{ref.name!r}')
                return

            # Should use iterative rather than recursive
            yield from self._topological_order(ref_task, seen)

        seen.add(task.name)
        yield task

    def _iterate_tasks(self, workflow):
        assert workflow._rn is None
        workflow._rn = self
        # yield workflowtask(workflow)

        for name in workflow._subworkflows:
            subworkflow = getattr(workflow, name)

            # Clean up hacky management of _rn attribute
            sub_rn = subworkflow._rn
            subworkflow._rn = None
            yield from sub_rn._iterate_tasks(subworkflow)

        for name in workflow._unbound_tasks:
            unbound_task = getattr(workflow, name)
            try:
                yield unbound_task.task()
            except UserWorkflowError as err:
                print_traceback(err.__cause__)
                continue
            # taskdir = self.directory / name
            # name = str(taskdir.relative_to(self._cache.directory))
            # task = unbound_task.task()

            # 1) encode to JSON and find dependencies
            # 2) make sure all dependencies exist (or recurse until they do)
            #
            # name = f'{self.prefix}/{name}'
            # task = unbound_task.bind(name, workflow)

            # Here we can also write down how to instantiate this workflow
            # object.  If that info is included in the task, then we can rerun
            # task generation upon request without the user needing to know
            # which workflow file generated it.

            # What do we actually need to do?
            # if task exists:
            #  - Task exists?
            #    - target or kwargs changed?
            #      - invalidate
            #      -
            #    - Invalidate if cha
            #  - If target or kwargs changed, invalidate
            #  - If task does not exist, add task.

            # (We probably need to yield and recurse dependencies.)

    def _add_task(self, task, dynamic_parent=None, force_overwrite=False):
        worker = None

        # XXX still contains duplication, should be split up etc.
        if task.dynamical:
            assert dynamic_parent is None
            assert not force_overwrite
            # How should we name the initial and final tasks?
            ini_name = f'{task.name}-ini'
            ini_task = Task(ini_name, task.target, task.kwargs, False)

            ini_node = ini_task._node(self._cache)
            action, indexnode = self._cache.add_or_update(ini_node, ini_name)
            ini_future = Future(ini_node, self._cache)
            print('{:>9s}: {}'.format(action, ini_future.describe()))

            fin_task = Task(
                task.name, 'define',
                kwargs={'obj': ini_future.output},
                dynamical=False)
            fin_node = fin_task._node(self._cache)
            action, indexnode = self._cache.add_or_update(fin_node, task.name)

            # fin_task needs to know that it depends on ini_task
            # in order to figure out its dependencies, a kind
            # of pre-initialization state.

            fin_future = Future(fin_node, self._cache)
            return fin_future

        node = task._node(self._cache, dynamic_parent=dynamic_parent)
        action, indexnode = self._cache.add_or_update(
            node, worker=worker,
            force_overwrite=force_overwrite)

        future = Future(node, self._cache)

        # if task.is_workflow:
        #     from taskblaster.state import State
        #     self._cache.registry.update_state(task.name, State.partial)

        import click
        colors = {'have': 'yellow',
                  'update': 'bright_yellow',
                  'add': 'bright_blue',
                  'conflict': 'bright_red',
                  'resolved': 'bright_blue'}

        # XXX fromdir=pwd
        print('{:>18s} {}'.format(
            click.style(action + ':', colors[action]),
            future.describe()))
        return future

    def _node(self, taskname, kwargs, name, dynamic_parent=None):
        # Raise an error if task cannot be imported:
        self._repo.import_task_function(taskname)
        return Node.new(self._cache.json_protocol, taskname, kwargs,
                        name, dynamic_parent=dynamic_parent)

    def task(self, target, name, **kwargs):
        node = self._node(target, kwargs, name)
        return self._task(node)

    def _task(self, node, *, dynamical=False):
        assert not Path(node.name).is_absolute()
        # TODO remove this (old) implementation

        # XXX dry-run
        # if unbound_task is None:
        #    worker = None
        #    dynamical = False
        # else:
        #    worker = unbound_task.worker
        #    dynamical = unbound_task.dynamical
        # worker = None

        if dynamical:
            # (XXX name is not equal to that of the node)
            ini_name = f'{name}/ini'
            action, indexnode = self._cache.add_or_update(node, ini_name)
            future = Future(node, self._cache)
            print('{:>9s}: {}'.format(action, future.describe()))

            # dyn parent?
            node = self._node('define', {'obj': future.output})

            fin_name = f'{name}/fin'
            action, indexnode = self._cache.add_or_update(node, fin_name)
            future2 = Future(node, self._cache)
            print('{:>9s}: {}'.format(action, future2.describe()))

            return future2

        action, indexnode = self._cache.add_or_update(node, worker=None)
        future = Future(node, self._cache)

        # XXX fromdir=pwd
        if not self._silent:
            print('{:>9s}: {}'.format(action, future.describe()))
        return future

    def define(self, obj, name):
        # XXX duplicates stuff from currently in __init__
        node = self._node('define', {'obj': obj}, name=name)
        return self._task(node)  # XXX no worker
