import pytest
from taskblaster.repository import Repository


class MyTasks:
    @staticmethod
    def hello(whom):
        return f'hello, {whom}!'

    @staticmethod
    def add(a, b):
        return a + b

    @staticmethod
    def multiply(a, b):
        return a * b


def workflow(rn):
    rn.task('hello')


@pytest.fixture
def newrepo(testdir):
    # Unlocked repository
    return Repository.create(testdir)


def test_workflow_hello(newrepo):
    newrepo._tasks['hello'] = MyTasks.hello

    rn = newrepo.runner()

    whom = 'world'

    with newrepo:
        hello = rn.task('hello', whom=whom, name='xxx')

    hello.run_blocking(newrepo)

    with newrepo:
        assert hello.has_output()
        record = hello.value()
    assert record.output == f'hello, {whom}!'


def test_workflow(newrepo):
    newrepo._tasks['add'] = MyTasks.add
    newrepo._tasks['multiply'] = MyTasks.multiply

    rn = newrepo.runner()

    with newrepo:
        five = rn.task('add', a=2, b=3, name='add')
        twenty = rn.task('multiply', a=five.output, b=4, name='mul')

    twenty.runall_blocking(newrepo)
    with newrepo:
        assert twenty.value().output == 20
