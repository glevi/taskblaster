import pytest
from taskblaster.cache import FileCache
from taskblaster.hashednode import Node
from taskblaster.storage import JSONProtocol
from taskblaster.state import State
from taskblaster.registry import Missing


@pytest.fixture
def cache(tmp_path, registry):
    cache = FileCache(directory=tmp_path / 'tree',
                      registry=registry,
                      json_protocol=JSONProtocol())
    with cache.registry.conn:
        yield cache


def mknode(target, dct, name):
    return Node.new(JSONProtocol(), target, dct, name)


@pytest.fixture
def nodes():
    return [mknode('hello', {}, 'a'),
            mknode('hello2', {}, 'b'),
            mknode('hello', {'a': 42}, 'c')]


def test_empty_cache(cache):
    assert len(cache) == 0
    assert '12345' not in cache
    assert len(list(cache)) == 0
    with pytest.raises(KeyError):
        cache['12345']


def test_cache_add(cache):
    """Test behaviour when adding a single node."""
    node = mknode('hello', {'a': 3, 'b': 4}, 'id')
    print(node.name)

    assert len(cache) == 0
    assert node.name not in cache
    action, indexnode = cache.add_or_update(node, 'test')
    assert action == 'add'
    assert len(cache) == 1

    assert indexnode.name in cache
    future1 = cache[node.name]
    assert future1.node.name == indexnode.name


def test_has_already(cache):
    node = mknode('hello', {}, 'id')
    cache.add_or_update(node, 'test')
    assert node.name in cache
    action, indexnode = cache.add_or_update(node, 'test')
    assert action == 'have'


def test_remove(cache, nodes):
    # We make 3 nodes.  Then we delete nodes[1] and verify that.

    for node in nodes:
        cache.add_or_update(node, node.name)

    n_initial = 3
    assert len(cache) == n_initial
    cache.delete_nodes([nodes[1]])
    n_remains = len(cache)
    assert n_remains == 2

    for i in range(3):
        exists = (i != 1)
        name = nodes[i].name
        entrydir = cache.directory / name
        assert (name in cache) == exists
        assert entrydir.exists() == exists

        if exists:
            entry = cache.entry(name)
            assert entry.node(name) == nodes[i]


def test_repr(cache):
    print(str(cache))
    print(repr(cache))


def test_finished(cache):
    node = mknode('func', {'x': 1}, 'n1')
    cache.add_or_update(node, 'func1')
    future = cache[node.name]  # Future(node, cache)
    node2 = mknode('func', {'x': future}, 'n2')
    cache.add_or_update(node2, 'func2')

    def node2_awaitcount():
        return cache.registry.index.node(node2.name).awaitcount

    assert node2_awaitcount() == 1
    cache.update_state(future._entry, State.done)
    assert node2_awaitcount() == 0
    cache.update_state(future._entry, State.fail)
    assert node2_awaitcount() == 1


def test_find_ready(cache):
    node = mknode('func', {}, 'nodename')
    cache.add_or_update(node)
    with pytest.raises(Missing):
        cache.find_ready()
    cache.registry.update_state(node.name, State.queue)
    indexnode = cache.find_ready()
    assert indexnode.name == node.name


def test_none_ready(cache):
    with pytest.raises(Missing):
        cache.find_ready()
