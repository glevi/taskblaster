import pytest
from taskblaster.entry import Flags


@pytest.fixture
def flags(tmp_path):
    return Flags(tmp_path / 'tmp.txt')


def test_empty(flags):
    flags.read() == []


def test_rw(flags):
    words = 'hello world'.split()
    flags.write(words)
    assert set(flags.read()) == set(words)


@pytest.mark.parametrize('word', ['hello world', 'hello\n'])
def test_bad(flags, word):
    with pytest.raises(ValueError):
        flags.write([word])


def test_remove(flags):
    flags.write(['hello'])
    assert flags.path.exists()
    flags.write([])
    assert not flags.path.exists()
