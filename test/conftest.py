import os
from pathlib import Path
import sqlite3

import pytest

from taskblaster.repository import Repository
from taskblaster.registry import Registry, Index


@pytest.fixture
def testdir(tmp_path):
    cwd = Path.cwd()
    try:
        os.chdir(tmp_path)
        yield tmp_path
    finally:
        os.chdir(cwd)
        print('tmp_path:', tmp_path)


@pytest.fixture
def repo(testdir):
    with Repository.create(testdir) as repo:
        yield repo


@pytest.fixture
def registry(tmp_path):
    return Registry(tmp_path / 'registry.dat')


@pytest.fixture
def namespace(tbdb):
    return tbdb.namespace


@pytest.fixture
def index(tmp_path):
    with sqlite3.connect(tmp_path / 'registry.db') as conn:
        yield Index.initialize(conn)
