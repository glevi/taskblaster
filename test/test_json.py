import pytest
from taskblaster.encoding import dumps, loads


def test_object_not_serializable():
    with pytest.raises(TypeError, match='Cannot encode object'):
        dumps(object())


class MyEncodable:
    def __init__(self, thing):
        self.thing = thing

    def tb_encode(self):
        return self.thing

    @classmethod
    def tb_decode(cls, objdata):
        return cls(objdata)


def test_json_roundtrip():
    obj = MyEncodable([1, 2, 'hello', {'x1': True, 'x2': None}])

    txt = dumps(obj)
    newobj = loads(txt)

    assert newobj.thing == obj.thing


@pytest.mark.parametrize('obj', [
    None, 'hello', 123, [1, 2, 4], {}, {'a': 'b', 'c': 'd'}, True, 1.2345
])
def test_json_different_builtins(obj):
    assert loads(dumps(obj)) == obj
